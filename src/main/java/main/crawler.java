/**
 * 
 */
package main;

import model.parameters.CommandParameters;

import com.beust.jcommander.JCommander;

import common.RequestConstants;
import spiders.A01;
import spiders.A02;
import spiders.A03;
import spiders.A04;
import spiders.A044;
import spiders.B02;
import spiders.SpiderBase;

/**
 * @author thi
 *
 */
public class crawler {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		CommandParameters params = new CommandParameters();
		new JCommander(params, args);
		int request = params.getRequest();
		SpiderBase spider = null;

		switch (request) {
		case RequestConstants.GET_CCY_LIST:
			spider = new A02();
			break;
		case RequestConstants.RUN_DAILY_CYY:
			spider = new A01(RequestConstants.RUN_DAILY_CYY, params);
			break;
		case RequestConstants.RUN_NEW_CCY:
			spider = new A01(RequestConstants.RUN_NEW_CCY, params);
			break;
		case RequestConstants.RUN_OLD_CYY:
			spider = new A01(RequestConstants.RUN_OLD_CYY, params);
			break;
		case RequestConstants.RUN_REQ_CCY_FAIL:
			spider = new A01(RequestConstants.RUN_REQ_CCY_FAIL, params);
			break;
		case RequestConstants.RUN_CCY:
			spider = new A01(RequestConstants.RUN_CCY, params);
			break;
		case RequestConstants.RUN_PNG:
			A03.getData(params);
			break;
		case RequestConstants.RUN_PNG_PARSE:
			A03.getData(params);
			break;		
			
		case RequestConstants.RUN_PNG_VERSION:
			A03.getData(params);
			break;		
		case RequestConstants.RUN_PNG_3:
			A03.getData(params);
			break;			
		case RequestConstants.COMMODITY_DAILY:
			spider = new A04(request,params);
			break;
		case RequestConstants.COMMODITY_HISTORY:
			spider = new A04(request,params);
			break;
		case RequestConstants.B02_NEWDATA:
			spider = new B02(RequestConstants.B02_NEWDATA,params);
			break;
		case RequestConstants.B02_OLDDATA:
			spider = new B02(RequestConstants.B02_OLDDATA,params);
			break;			
		default:
			break;
		}

		if (spider != null) {
			spider.run();
		}
	}

}
