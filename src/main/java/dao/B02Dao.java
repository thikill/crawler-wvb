package dao;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import com.beust.jcommander.internal.Console;

import model.B02.*;

public class B02Dao extends MySQLBaseDao {
	public int getProductIDByURL(String url) {
		int productID = 0;
		open();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery(String.format("SELECT * FROM crawl.product where producturl =\"%s\" ", url));

			while (resultSet.next()) {
				productID = Integer.parseInt(resultSet.getString("id"));
				break;
			}
			resultSet.close();
			statement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return productID;
	}

	public List<Product> getListOldProduct() {
		List<Product> listProduct = new ArrayList<Product>();
		open();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("select * from product a where a.id not in (select id from json)");

			while (resultSet.next()) {
				Product product = new Product();
				product.setId(resultSet.getString("id"));
				product.setUrl(resultSet.getString("producturl"));
				listProduct.add(product);
			}
			resultSet.close();
			statement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return listProduct;
	}

	public boolean chkProductIDExisted(int productID) {
		boolean result = false;
		open();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement
					.executeQuery("SELECT count(*) as countProduct FROM crawl.json where id = " + productID);

			while (resultSet.next()) {
				if (Integer.parseInt(resultSet.getString("countProduct")) > 0) {
					result = true;
				}
				break;
			}
			resultSet.close();
			statement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return result;
	}

	public boolean chkProductURLExisted(String productURL) {
		boolean result = false;
		open();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery(
					"SELECT count(*) as countProduct FROM crawl.json where product_url = \"" + productURL + "\"");

			while (resultSet.next()) {
				if (Integer.parseInt(resultSet.getString("countProduct")) > 0) {
					result = true;
				}
				break;
			}
			resultSet.close();
			statement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		return result;
	}

	public void insert(int id, String value, int type, String productURL) {
		open();
		try {
			if (id == 0) {
				Statement statement = connection.createStatement();
				ResultSet resultSet = statement.executeQuery(
						"select max(a.id) as  id from (select (max(id)+1) as id from product union select (max(id)+1) as id from json) a;");

				while (resultSet.next()) {
					id = Integer.parseInt(resultSet.getString("id"));
					break;
				}
				resultSet.close();
				statement.close();
			}
			PreparedStatement preparedStatement = connection
					.prepareStatement("INSERT INTO crawl.json (id,value,type, product_url) VALUES (?,?,?,?)");
			preparedStatement.setInt(1, id);
			preparedStatement.setString(2, value);
			preparedStatement.setInt(3, type);
			preparedStatement.setString(4, productURL);
			preparedStatement.executeUpdate();
			preparedStatement.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		close();

	}

	public List<JsonModel> getListJson() {
		List<JsonModel> listJson = new ArrayList<JsonModel>();
		open();
		try {
			Statement statement = connection.createStatement();
			ResultSet resultSet = statement.executeQuery("select * from json");

			while (resultSet.next()) {
				JsonModel jsonModel = new JsonModel();
				jsonModel.setId(resultSet.getInt("id"));
				jsonModel.setValue(resultSet.getString("value"));
				jsonModel.setQuantity(resultSet.getString("quantity"));
				listJson.add(jsonModel);
			}
			resultSet.close();
			statement.close();

		} catch (SQLException e) {
			e.printStackTrace();
		}
		close();
		int count = 0;
		for (JsonModel jsonModel : listJson) {
			/*
			 * count++; if (count<1198) continue;
			 */

			String oldJson = jsonModel.getValue();
			// System.out.println("oldJson==" + oldJson);
			String url = StringUtils.substringBetween(oldJson, ",\"url\":\"", "\",\"images\":");
			// System.out.println(url);
			String arrJson1 = StringUtils.substringBetween(oldJson, "\"images\":", ",\"choices\":");
			arrJson1 = arrJson1.replace("imageSource", "imagesource");
			arrJson1 = arrJson1.replace("attributeName", "attributename");
			arrJson1 = arrJson1.replace("attributeValue", "attributevalue");
			// System.out.println("arrJson1==" + arrJson1);

			String arrJson2 = StringUtils.substringBetween(oldJson, ",\"choices\":", "}]}");
			arrJson2 = arrJson2.replace("choiceName", "choicename");
			// System.out.println("arrJson2==" + arrJson2);

			String listColor = StringUtils.substringBetween(oldJson, "\"--Select Color--\",\"options\":", "}]}");
			String colors;
			String newJson;

			if (listColor != null) {
				colors = "[{\"name\":\"PMS Process Black\",\"value\":\"#000000\"},{\"name\":\"Navy PMS 289\",\"value\":\"#0C2340\"},{\"name\":\"PMS Process Blue\",\"value\":\"#0085CA\"},{\"name\":\"PMS Reflex Blue\",\"value\":\"#001489\"},{\"name\":\"Royal PMS 300\",\"value\":\"#005EB8\"},{\"name\":\"Brown PMS 161\",\"value\":\"#603D20\"},{\"name\":\"Burgundy PMS 222\",\"value\":\"#6C1D45\"},{\"name\":\"Gold Athletic PMS 123\",\"value\":\"#FFC72C\"},{\"name\":\"Gold Metallic PMS 874\",\"value\":\"#ffd700\"},{\"name\":\"Gray PMS 430\",\"value\":\"#7C878E\"},{\"name\":\"Green Forest PMS 357\",\"value\":\"#215732\"},{\"name\":\"Green Kelly PMS 354\",\"value\":\"#00B140\"},{\"name\":\"Maroon PMS 202\",\"value\":\"#862633\"},{\"name\":\"Orange PMS 021\",\"value\":\"#FE5000\"},{\"name\":\"Pink PMS 210\",\"value\":\"#F99FC9\"},{\"name\":\"Purple PMS Violet\",\"value\":\"#440099\"},{\"name\":\"Red PMS 186\",\"value\":\"#C8102E\"},{\"name\":\"Red Rhodamine PMS\",\"value\":\"#E10098\"},{\"name\":\"Red Rubine PMS\",\"value\":\"#CE0058\"},{\"name\":\"Red Warm PMS W. Red\",\"value\":\"#F9423A\"},{\"name\":\"Silver Metallic PMS 877\",\"value\":\"#c0c0c0\"},{\"name\":\"Teal PMS 320\",\"value\":\"#009CA6\"},{\"name\":\"White PMS White\",\"value\":\"#ffffff\"}]";
				arrJson2 = arrJson2.replace(listColor, colors);

			}
			if (jsonModel.getQuantity() != null) {
				newJson = "{\"images\":" + arrJson1 + ",\"choices\":" + arrJson2 + "}]" + ",\"size\":"
						+ jsonModel.getQuantity() + "}";
				 System.out.println(jsonModel.getId() + "==" + newJson);
			} else {
				newJson = "{\"images\":" + arrJson1 + ",\"choices\":" + arrJson2 + "}]}";
				 System.out.println(jsonModel.getId() + "==" + newJson);
			}
			updateJson(jsonModel.getId(), newJson);
			// System.out.println(jsonModel.getId() + "==" + newJson);
			count++;
			// if (count>1) break;
			// if (count>1204) break;

		}
		return listJson;
	}

	public void updateJson(int productID, String newValue) {

		PreparedStatement preparedStatement = null;

		String updateTableSQL = "UPDATE json SET value = ? " + " WHERE id = ?";

		try {
			open();
			preparedStatement = connection.prepareStatement(updateTableSQL);

			preparedStatement.setInt(2, productID);
			preparedStatement.setString(1, newValue);

			// execute update SQL stetement
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			close();

		}
		close();
	}

	public void updateQuantity(int productID, String value) {

		PreparedStatement preparedStatement = null;

		String updateTableSQL = "UPDATE json SET quantity = ? " + " WHERE id = ?";

		try {
			open();
			preparedStatement = connection.prepareStatement(updateTableSQL);

			preparedStatement.setInt(2, productID);
			preparedStatement.setString(1, value);

			// execute update SQL stetement
			preparedStatement.executeUpdate();

		} catch (SQLException e) {

			System.out.println(e.getMessage());

		} finally {

			if (preparedStatement != null) {
				try {
					preparedStatement.close();
				} catch (SQLException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}

			close();

		}
		close();
	}

	public static void main(String[] args) {
		B02Dao dao = new B02Dao();
		// System.out.println(dao.getProductIDByURL("http://www.swagstandard.com/wave-zippered-padfolio-1.html"));
		// dao.insert(0,"fdfd",1);
		// System.out.println(dao.chkProductIDExisted(451));
		// List<Product> products = dao.getListOldProduct();
		// System.out.println(dao.getListJson().size());
		dao.getListJson();
		// dao.updateJson(1, "newValue");
		// dao.updateJson(1, null);
		int a;
	}
}
