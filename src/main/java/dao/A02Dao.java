package dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import model.CCY;
import model.CurrencyRate;
import model.crawler.RequestFail;
import model.crawler.Spider;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.table.TableUtils;

public class A02Dao extends BaseDao {
	Dao<CCY, String> ccyDao;
	final static Logger logger = LogManager.getLogger(A02Dao.class);
	String dbName;

	public A02Dao() {
	}

	private void open() {
		try {
			connectionSource = getConnection();
			ccyDao = DaoManager.createDao(connectionSource, CCY.class);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void createTables() {

		try {
			open();
			TableUtils.createTableIfNotExists(connectionSource, CCY.class);
			TableUtils.dropTable(connectionSource, RequestFail.class, true);
			TableUtils.createTable(connectionSource, RequestFail.class);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}

	public List<CCY> getListCCY() {
		List<CCY> listCCY = null;
		try {
			open();
			ccyDao = DaoManager.createDao(connectionSource, CCY.class);
			listCCY = ccyDao.queryForAll();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
		return listCCY;
	}

	public List<CCY> getListNewCCY() {
		List<CCY> listCCY = null;
		try {
			open();
			listCCY = ccyDao.queryForEq("type", 1);
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
		return listCCY;
	}

	public void updateCCYStatus(CCY ccy) {
		try {
			open();
			ccy.setType(0);
			//ccyDao.update(ccy);
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}

	}

	public void createCCY(CCY ccy) {
		try {
			open();
			ccyDao.create(ccy);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}

	public boolean checkReqExisted(CCY ccy) {
		boolean existed = false;
		try {
			open();
			existed = ccyDao.queryBuilder().where().eq("value", ccy.getValue())
					.countOf() > 0;
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
		return existed;
	}
}
