package dao;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import model.CCY;

public class CommonDAO {
	A02Dao a02Dao;
	final static Logger logger = LogManager.getLogger(CommonDAO.class);

	public CommonDAO() {
		a02Dao = new A02Dao();
	}

	public List<CCY> getListNewCCY() {
		return a02Dao.getListNewCCY();
	}
	
	public List<CCY> getListCCY() {
		return a02Dao.getListCCY();
	}


	public void updateCCYStatus(CCY ccy) {
		a02Dao.updateCCYStatus(ccy);
	}

}
