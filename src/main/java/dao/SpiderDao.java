/**
 * 
 */
package dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import utils.FileUtils;
import model.CurrencyRate;
import model.crawler.RequestFail;
import model.crawler.Spider;
import model.crawler.SpiderInstance;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.jdbc.JdbcConnectionSource;
import com.j256.ormlite.support.ConnectionSource;

/**
 * @author thi
 *
 */
public class SpiderDao extends BaseDao {
	private final static String DATABASE_NAME = "crawler_db";

	Dao<Spider, String> spiderDao;
	Dao<SpiderInstance, String> spiderInstanceDao;
	Dao<RequestFail, String> reqFailDao;
	ConnectionSource connectionSource = null;
	final static Logger logger = LogManager.getLogger(SpiderDao.class);

	public SpiderDao() {
	}

	private void open() {
		try {
			dbName = this.getClass().getSimpleName().replace("Dao", "");
			connectionSource = getConnection(FileUtils
					.getFileSqlite(DATABASE_NAME));
			spiderDao = DaoManager.createDao(connectionSource, Spider.class);

			spiderInstanceDao = DaoManager.createDao(connectionSource,
					SpiderInstance.class);

			reqFailDao = DaoManager.createDao(connectionSource,
					RequestFail.class);
		} catch (SQLException e) {
			logger.error(e.getMessage());
		}

	}

	public void createSpider(Spider spider) {
		try {
			open();
			spiderDao.create(spider);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}

	public void createReqFail(RequestFail reqFail) {
		try {
			open();
			reqFailDao.create(reqFail);
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
	}

	public int getSpiderIDByName(String name) {
		int spiderID = 0;
		try {
			open();
			spiderID = spiderDao.queryForEq("name", name).get(0).getId();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
		return spiderID;

	}

	public void createSpiderInstance(SpiderInstance spiderInstance) {
		try {
			open();
			spiderInstanceDao.create(spiderInstance);
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
	}

	public void updateSpiderInstance(SpiderInstance spiderInst) {
		try {
			open();
			spiderInstanceDao.update(spiderInst);
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
	}

	public boolean checkSpiderExisted(String name) {
		boolean existed = false;
		try {
			open();
			existed = spiderDao.queryBuilder().where().eq("name", name)
					.countOf() > 0;

			// System.exit(0);
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
		return existed;
	}

	public boolean checkReqExisted(String url, int spiderID) {
		boolean existed = false;
		try {
			open();
			existed = reqFailDao.queryBuilder().where().eq("url_request", url)
					.and().eq("spider_id", spiderID).countOf() > 0;
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
		return existed;
	}

	public List<RequestFail> getListReqFailed() {
		List<RequestFail> listReq = null;
		try {
			open();
			listReq = reqFailDao.queryForAll();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
		return listReq;
	}

	public static void main(String[] args) {
		SpiderDao dao = new SpiderDao();
		boolean check = dao.checkReqExisted("a", 1);
		System.out.println(dao.getListReqFailed().size());
	}

}