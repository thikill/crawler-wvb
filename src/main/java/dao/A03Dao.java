package dao;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import model.A03Model;

public class A03Dao {
	public static Connection getOracleConnection() {
		String driver = "oracle.jdbc.driver.OracleDriver";
		// String url = "jdbc:oracle:thin:@203.128.247.186:1521:wvbhn";
		String url = "jdbc:oracle:thin:@64.244.240.198:1521:wvbprod";
		String username = "WVBXBRL";
		String password = "xbrl4wvb12";
		Connection conn = null;

		try {
			Class.forName(driver); // load Oracle driver
			conn = DriverManager.getConnection(url, username, password);
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return conn;
	}

	public static List<A03Model> getListCompany(int from, int to)
			throws SQLException {
		Connection con = getOracleConnection();
		List<A03Model> result = new ArrayList<A03Model>();

		Statement stmt = null;
		String query = "select * from PG_COMPANIES_PRE where status = 0 and regist_name not like '3-%' and	ROW_NO>="
				+ from + "  and ROW_NO<=" + to + " order by row_no asc";

		try {
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				A03Model model = new A03Model();
				model.id = rs.getInt("ROW_NO");
				model.companyId = rs.getString("REGIST_NAME");
				result.add(model);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				con.close();
			}
			if (stmt != null) {

				stmt.close();

			}
		}
		return result;
	}
	
	public static List<A03Model> getListCompanySecond(int from, int to)
			throws SQLException {
		Connection con = getOracleConnection();
		List<A03Model> result = new ArrayList<A03Model>();

		Statement stmt = null;
		String query = "select * from PG_COMPANIES_PRE where status = 0 and regist_name  like '3-%' and	ROW_NO>="
				+ from + "  and ROW_NO<=" + to + " order by row_no asc";

		try {
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				A03Model model = new A03Model();
				model.id = rs.getInt("ROW_NO");
				model.companyId = rs.getString("REGIST_NAME");
				result.add(model);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				con.close();
			}
			if (stmt != null) {

				stmt.close();

			}
		}
		return result;
	}	
	
	public static List<A03Model> getListCompanyForExtract(int from, int to)
			throws SQLException {
		Connection con = getOracleConnection();
		List<A03Model> result = new ArrayList<A03Model>();

		Statement stmt = null;
		String query = "select * from PG_COMPANIES_PRE where EXTRACT_STATUS = 0  and regist_name  not like '3-%' and	ROW_NO>="
				+ from + "  and ROW_NO<=" + to + " order by row_no asc";

		try {
			stmt = con.createStatement();
			ResultSet rs = stmt.executeQuery(query);

			while (rs.next()) {
				A03Model model = new A03Model();
				model.id = rs.getInt("ROW_NO");
				model.companyId = rs.getString("REGIST_NAME");
				result.add(model);
			}
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			if (con != null) {
				con.close();
			}
			if (stmt != null) {

				stmt.close();

			}
		}
		return result;
	}
	
	
	

	public static void UpdateStatus(String id, int status) throws SQLException {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = getOracleConnection();

			pstmt = con.prepareStatement("UPDATE PG_COMPANIES_PRE"
					+ " SET STATUS = ? " + "WHERE REGIST_NAME = ?");

			pstmt.setInt(1, status);
			pstmt.setString(2, id);
			pstmt.executeUpdate();
		} finally {
			if (pstmt != null)
				pstmt.close();
			if (con != null) {
				con.close();
			}
		}
	}
	
	public static void UpdateExtractStatus(String id, int status) throws SQLException {

		Connection con = null;
		PreparedStatement pstmt = null;

		try {
			con = getOracleConnection();

			pstmt = con.prepareStatement("UPDATE PG_COMPANIES_PRE"
					+ " SET EXTRACT_STATUS = ? " + "WHERE REGIST_NAME = ?");

			pstmt.setInt(1, status);
			pstmt.setString(2, id);
			pstmt.executeUpdate();
		} finally {
			if (pstmt != null)
				pstmt.close();
			if (con != null) {
				con.close();
			}
		}
	}

	public static void main(String[] args) {
		Connection con = getOracleConnection();
		try {
			UpdateStatus("1-103002",0);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		int x = 0;
	}
}
