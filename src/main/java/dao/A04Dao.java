package dao;

import java.sql.SQLException;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import model.A04Model;
import model.crawler.RequestFail;

import com.j256.ormlite.dao.Dao;
import com.j256.ormlite.dao.DaoManager;
import com.j256.ormlite.table.TableUtils;

public class A04Dao extends BaseDao {
	Dao<A04Model, String> dao;
	final static Logger logger = LogManager.getLogger(A04Dao.class);
	String dbName;

	public A04Dao() {
	}

	private void open() {
		try {
			connectionSource = getConnection();
			dao = DaoManager.createDao(connectionSource, A04Model.class);
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	public void createTables() {

		try {
			open();
			TableUtils.dropTable(connectionSource, A04Model.class, true);
			TableUtils.createTable(connectionSource, A04Model.class);
			TableUtils.dropTable(connectionSource, RequestFail.class, true);
			TableUtils.createTable(connectionSource, RequestFail.class);
		} catch (SQLException e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}

	public List<A04Model> getListData() {
		List<A04Model> listData = null;
		try {
			open();
			dao = DaoManager.createDao(connectionSource, A04Model.class);
			listData = dao.queryForAll();
		} catch (Exception e) {
			logger.error(e.getMessage());
		} finally {
			close();
		}
		return listData;
	}

	public void createData(A04Model model) {
		try {
			open();
			dao.create(model);
		} catch (Exception e) {
			e.printStackTrace();
		} finally {
			close();
		}
	}

}
