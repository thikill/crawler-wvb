package dao;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
public class MySQLBaseDao {
	Connection connection = null;
    public Connection open(){
        try {
            Class.forName("com.mysql.jdbc.Driver");
            if(connection == null)
                connection = DriverManager.getConnection("jdbc:mysql://localhost/crawl?user=root&password=123456");
 
        } catch (ClassNotFoundException e) {
 
            e.printStackTrace();
             
        } catch (SQLException e) {
             
            e.printStackTrace();
             
        }
        return connection;
    }
    
    public void close() {
    	if (connection != null) {
    		try {
				connection.close();
				connection = null;
			} catch (SQLException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
    	}
    }
}
