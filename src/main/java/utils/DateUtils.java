/**
 * 
 */
package utils;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * @author thi
 *
 */
public class DateUtils {
	public static String DATE_FORMAT = "yyyy-MM-dd";
	public static String TIME_FORMAT = "yyyy-MM-dd HH:mm:ss";
	public static SimpleDateFormat dateFormat = new SimpleDateFormat(
			DATE_FORMAT);
	public static SimpleDateFormat timeFormat = new SimpleDateFormat(
			TIME_FORMAT);

	public static String getDateInString(Date date) {
		return dateFormat.format(date);
	}

	public static final TimeZone GMT_TIME_ZONE = TimeZone.getTimeZone("GMT");

	public static String getDateInString(long date) {
		// TimeZone.setDefault(GMT_TIME_ZONE);
		dateFormat.setTimeZone(GMT_TIME_ZONE);
		return dateFormat.format(new Date(date));
	}

	public static String getTimeInString(Date time) {
		return timeFormat.format(time);
	}

	public static int getIntervalTime(String fromTime, String toTime) {
		long time = 0;
		try {
			Date fromDate = timeFormat.parse(fromTime);
			Date toDate = timeFormat.parse(toTime);
			time = Math.abs(toDate.getTime() - fromDate.getTime()) / 1000;
		} catch (ParseException e) {
			e.printStackTrace();
		}

		return (int) time;
	}

	public static void addDays(Date d, int days) {
		Calendar c = Calendar.getInstance();
		c.setTime(d);
		c.add(Calendar.DATE, days);
		d.setTime(c.getTime().getTime());
	}

	public static String getStringByDate(Date date, String datePattern) {
		Format formatter = new SimpleDateFormat(datePattern);
		String currDate = formatter.format(date);
		return currDate;
	}

	public static String getStringByDate(Date date) {
		Format formatter = new SimpleDateFormat(DATE_FORMAT);
		String currDate = formatter.format(date);
		return currDate;
	}

	public static Date getDateFromString(String date, String datePattern) {
		if (date == null)
			return null;
		DateFormat formatter = new SimpleDateFormat(datePattern);
		Date currDate = null;
		try {
			currDate = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return currDate;
	}

	public static Date getDateFromString(String date) {
		if (date == null)
			return null;
		DateFormat formatter = new SimpleDateFormat(DATE_FORMAT);
		Date currDate = null;
		try {
			currDate = formatter.parse(date);
		} catch (ParseException e) {
			e.printStackTrace();
		}
		return currDate;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		System.out.println(getTimeInString(new Date()));
	}

	public static String convertToOtherFormat(String dateValue,
			String newPattern) {
		SimpleDateFormat format2;
		Date date;
		try {
			SimpleDateFormat format1 = new SimpleDateFormat(DATE_FORMAT);
			format2 = new SimpleDateFormat(newPattern);
			date = format1.parse(dateValue);
			return format2.format(date);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}

	}

}
