/**
 * 
 */
package utils;

import java.io.File;
import java.io.IOException;
import java.net.URISyntaxException;
import java.security.CodeSource;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dao.BaseDao;

/**
 * @author thi
 *
 */
public class FileUtils {
	final static Logger logger = LogManager.getLogger(FileUtils.class);

	public static String getFullPath(Object obj, String uri) {
		String result = null;
		ClassLoader classLoader = obj.getClass().getClassLoader();
		result = classLoader.getResource(uri).getFile();
		return result;
	}

	public static File getFileResource(Object obj, String uri) {
		String result = null;
		ClassLoader classLoader = obj.getClass().getClassLoader();
		if (classLoader.getResource(uri) == null)
			return null;
		result = classLoader.getResource(uri).getFile();
		return new File(result);
	}

	public static String getFileSqlite(String name) {
		String result = null;

		result = "jdbc:sqlite:" + getJarDir() + "/resources/data/" + name
				+ ".db";
		return result;
	}

	public static String getSettingFile(String name) {
		String result = null;
		result = getJarDir() + "/resources/settings/" + name +".properties";
		return result;
	}

	public static String getJarDir() {
		String jarDir = null;
		try {
			CodeSource codeSource = main.crawler.class.getProtectionDomain()
					.getCodeSource();
			File jarFile = new File(codeSource.getLocation().toURI().getPath());
			jarDir = jarFile.getParentFile().getPath();
			// logger.info("jarDir==" + jarDir);
		} catch (URISyntaxException e) {
			logger.error(e.getMessage());
		}
		return jarDir;
	}


	public static String getFullPathSqlite(Object obj, String uri) {
		String result = null;
		ClassLoader classLoader = obj.getClass().getClassLoader();
		result = "jdbc:sqlite:" + classLoader.getResource(uri).getFile();
		return result;
	}
	
	
}
