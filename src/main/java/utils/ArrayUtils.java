package utils;

import java.util.ArrayList;
import java.util.List;

import model.CCY;

public class ArrayUtils {
	public static int[] getIntArrFromStringArr(String[] stringArr) {
		int[] numbers = new int[stringArr.length];
		for (int i = 0; i < stringArr.length; i++) {
			numbers[i] = Integer.parseInt(stringArr[i]);
		}
		return numbers;
	}

}
