/**
 * 
 */
package utils;

import java.util.Properties;

import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.AddressException;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import spiders.A01;
import model.settings.MailSetting;

/**
 * @author thi
 *
 */
public class SendMailUtils {
	static Logger logger = LogManager.getLogger(SendMailUtils.class.getName());

	public static void sendMail(MailSetting setting, String subject, String msg) {
		try {
			final String username = setting.getUserName();
			final String password = setting.getPassword();

			Properties props = new Properties();
			props.put("mail.smtp.auth", setting.isAuth());
			props.put("mail.smtp.starttls.enable", setting.isStarttls());
			props.put("mail.smtp.host", setting.getHost());
			props.put("mail.smtp.port", setting.getPort());

			Session session = Session.getInstance(props,
					new javax.mail.Authenticator() {
						protected PasswordAuthentication getPasswordAuthentication() {
							return new PasswordAuthentication(username,
									password);
						}
					});

			Message message = new MimeMessage(session);
			message.setFrom(new InternetAddress(setting.getFrom()));
			message.setRecipients(Message.RecipientType.TO,
					InternetAddress.parse(setting.getTo()));
			message.setSubject(subject);
			message.setText(msg);

			Transport.send(message);

		} catch (MessagingException e) {
			logger.error(e);
		} catch (Exception e) {
			logger.error(e);
		}

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {

	}

}
