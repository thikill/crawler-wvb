/**
 * 
 */
package utils;

import java.util.List;

import model.A03KeyValue;

/**
 * @author thi
 *
 */
public class StringUtils {

	public static int getIndexByKey(List<A03KeyValue> listData, String key) {
		int index = 0;

		for (int i = 0; i < listData.size(); i++) {

			if (listData.get(i).getKey().equals(key)) {
				index = i;
				break;
			}

		}

		return index;

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

}
