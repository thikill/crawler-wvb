package common;

import model.crawler.Spider;
import model.settings.Concurrent;
import model.settings.Connection;
import model.settings.RequestHeader;
import model.settings.Setting;
import model.settings.SpiderInfo;

import org.apache.commons.configuration.Configuration;

import utils.ArrayUtils;

public class SettingCommon {

	public static Setting getSpiderSetting(Configuration config) {
		Setting setting = new Setting();
		Concurrent concurrent = new Concurrent();
		concurrent.setRequestPerDomain(config.getInt(
				"CONCURRENT.REQUESTS_PER_DOMAIN", -1));
		concurrent.setRequestPerIP(config.getInt("CONCURRENT.REQUESTS_PER_IP",
				-1));

		Connection conn = new Connection();
		if (config.containsKey("CONNECTION.DELAY")) {
			conn.setDelay((int) (1000 * config.getDouble("CONNECTION.DELAY")));
		} else {
			conn.setDelay(-1);
		}

		conn.setEnabled(config.getBoolean("CONNECTION.RETRY_ENABLED", false));

		if (config.containsKey("CONNECTION.TIMEOUT")) {
			conn.setTimeout(config.getInt("CONNECTION.TIMEOUT") * 1000);
		} else {
			conn.setTimeout(-1);
		}

		conn.setTimes(config.getInt("CONNECTION.RETRY_TIMES", -1));
		conn.setTargetWaitTime(config.getInt("CONNECTION.TARGET_WAIT_TIME", -1));
		conn.setHttpCodes(ArrayUtils.getIntArrFromStringArr(config
				.getStringArray("CONNECTION.RETRY_HTTP_CODES")));
		conn.setMaxFailed(config.getInt("CONNECTION.MAX_FAILED", -1));

		RequestHeader reqHeader = new RequestHeader();
		reqHeader.setAccept(config.getString("REQUEST_HEADERS.Accept", null));
		reqHeader.setAcceptEncoding(config.getString(
				"REQUEST_HEADERS.Accept-Encoding", null));
		reqHeader.setAcceptLanguage(config.getString(
				"REQUEST_HEADERS.Accept-Language", null));
		reqHeader.setUserAgent(config.getString("REQUEST_HEADERS.User-Agent",
				null));
		reqHeader.setxRequestedWith(config.getString(
				"REQUEST_HEADERS.X-Requested-With", null));
		reqHeader.setReferer(config.getString("REQUEST_HEADERS.Referer", null));
		reqHeader.setIgnoreContentType(config.getBoolean(
				"REQUEST_HEADERS.IgnoreContentType", false));
		reqHeader.setConnectionType(config
				.getString("REQUEST_HEADERS.Connection"));

		SpiderInfo spider = new SpiderInfo();
		spider.setStartURL(config.getString("spider.start_url"));
		spider.setQueryString(config.getString("spider.query_string"));

		setting.setConcurrent(concurrent);
		setting.setConnection(conn);
		setting.setRequestHeader(reqHeader);
		setting.setSpider(spider);
		setting.setDnsCacheEnabled(config.getBoolean("DNSCACHE_ENABLED", false));

		return setting;

	}

	public static void mergeSetting(Setting childSetting, Setting parentSetting) {
		Concurrent childConcurrent = childSetting.getConcurrent();
		Concurrent parentConcurrent = parentSetting.getConcurrent();
		if (childConcurrent.getRequestPerDomain() == -1) {
			childConcurrent.setRequestPerDomain(parentConcurrent
					.getRequestPerDomain());
		}
		if (childConcurrent.getRequestPerIP() == -1) {
			childConcurrent.setRequestPerIP(parentConcurrent.getRequestPerIP());
		}

		Connection childCon = childSetting.getConnection();
		Connection parentCon = parentSetting.getConnection();
		if (childCon.getMaxFailed() == -1) {
			childCon.setMaxFailed(parentCon.getMaxFailed());
		}
		if (childCon.getTimeout() == -1) {
			childCon.setTimeout(parentCon.getTimeout());
		}
		if (childCon.getTimes() == -1) {
			childCon.setTimes(parentCon.getTimes());
		}

		if (childCon.getDelay() == -1) {
			childCon.setDelay(parentCon.getDelay());
		}

		if (childCon.getTargetWaitTime() == -1) {
			childCon.setTargetWaitTime(parentCon.getTargetWaitTime());
		}

		if (childCon.getHttpCodes() == null
				|| childCon.getHttpCodes().length == 0) {
			childCon.setHttpCodes(parentCon.getHttpCodes());
		}

		RequestHeader childReqHeader = childSetting.getRequestHeader();
		RequestHeader parentReqHeader = parentSetting.getRequestHeader();

		if (childReqHeader.getAccept() == null) {
			childReqHeader.setAccept(parentReqHeader.getAccept());
		}

		if (childReqHeader.getAcceptEncoding() == null) {
			childReqHeader.setAcceptEncoding(parentReqHeader
					.getAcceptEncoding());
		}

		if (childReqHeader.getAcceptLanguage() == null) {
			childReqHeader.setAcceptLanguage(parentReqHeader
					.getAcceptLanguage());
		}

		if (childReqHeader.getUserAgent() == null) {
			childReqHeader.setUserAgent(parentReqHeader.getUserAgent());
		}

		if (childReqHeader.getxRequestedWith() == null) {
			childReqHeader.setxRequestedWith(parentReqHeader
					.getxRequestedWith());
		}
		if (childReqHeader.getReferer() == null) {
			childReqHeader.setReferer(parentReqHeader.getReferer());
		}

		if (childReqHeader.getConnectionType() == null) {
			childReqHeader.setConnectionType(parentReqHeader
					.getConnectionType());
		}

		childSetting.setConcurrent(childConcurrent);
		childSetting.setConnection(childCon);
		childSetting.setRequestHeader(childReqHeader);

		if (childSetting.isDnsCacheEnabled() == false) {
			childSetting.setDnsCacheEnabled(parentSetting.isDnsCacheEnabled());
		}

	}

}
