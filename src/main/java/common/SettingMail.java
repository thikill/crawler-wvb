package common;

import model.settings.MailSetting;

import org.apache.commons.configuration.Configuration;

public class SettingMail {

	public static MailSetting getMailSetting(Configuration config) {
		MailSetting setting = new MailSetting();

		setting.setAuth(config.getBoolean("smtp.auth"));
		setting.setFrom(config.getString("from"));
		setting.setHost(config.getString("smtp.host"));
		setting.setNotifyDone(config.getBoolean("notify.done"));
		setting.setNotifyDoneSubject(config.getString("crawler.done.subject"));
		setting.setNotifyFail(config.getBoolean("notify.fail"));
		setting.setNotifyFailSubject(config.getString("nofify.fail.subject"));
		setting.setPassword(config.getString("smtp.password"));
		setting.setPort(config.getString("smtp.port"));
		setting.setTo(config.getString("to"));
		setting.setStarttls(config.getBoolean("smtp.starttls.enable"));
		setting.setUserName(config.getString("smtp.username"));
		setting.setNotifyDoneMessage(config.getString("crawler.done.message"));
		setting.setNotifyFailMessage(config.getString("nofify.fail.message"));

		return setting;

	}

}
