package common;

public class RequestConstants {
	public static final int GET_CCY_LIST=1;
	public static final int RUN_NEW_CCY=2;
	public static final int RUN_OLD_CYY=3;
	public static final int RUN_DAILY_CYY=4;
	public static final int RUN_REQ_CCY_FAIL=5;
	public static final int RUN_CCY=6;
	public static final int RUN_PNG=7;
	public static final int COMMODITY_DAILY=8;
	
	public static final int RUN_PNG_VERSION=10;
	public static final int RUN_PNG_3=11;
	public static final int RUN_PNG_PARSE=9;
	
	public static final int COMMODITY_HISTORY=12;
	
	public static final int B02_NEWDATA=13;
	public static final int B02_OLDDATA=14;
}
