package exporters.converter;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;

import spiders.A01;
import utils.DateUtils;
import model.A04Model;
import model.CCY;
import model.CurrencyRate;
import exporters.csv.CommodityModel;
import exporters.oracle.model.OandaCurrencyRates;

public class Converter {

	public static OandaCurrencyRates convertCCYRate(CurrencyRate ccyRate) {
		OandaCurrencyRates objInfo = new OandaCurrencyRates();
		objInfo.setIsoCurrencyCode(ccyRate.getQuoteCurrency());
		objInfo.setRate(new BigDecimal(ccyRate.getRate()));
		objInfo.setRateDate(DateUtils.getDateFromString(ccyRate.getDate(),
				A01.datePattern));
		return objInfo;
	}

	public static List<OandaCurrencyRates> convertListCCYRate(
			List<CurrencyRate> listCCYRate) {
		List<OandaCurrencyRates> listResult = new ArrayList<OandaCurrencyRates>();
		for (CurrencyRate ccyRate : listCCYRate) {
			listResult.add(convertCCYRate(ccyRate));
		}
		return listResult;
	}

	public static CommodityModel convertCommodity(A04Model spiderModel) {
		CommodityModel model = new CommodityModel();
		model.setTime(spiderModel.getTime());
		model.setCcyCode(spiderModel.getCcyCode());
		model.setPrice(spiderModel.getPrice());
		model.setName(spiderModel.getTitle().split("-")[0].replaceAll(
				"Futures", "").trim());
		model.setCode(StringUtils.substringBetween(spiderModel.getTitle()
				.split("-")[1].replaceAll("Futures", "").trim(), "(", ")"));
		model.setUnit(spiderModel.getUnit());
		return model;
	}

	public static List<CommodityModel> convertListA04Model(
			List<A04Model> listSpiderModel) {
		List<CommodityModel> listModel = new ArrayList<CommodityModel>();
		for (A04Model a04Model : listSpiderModel) {
			listModel.add(convertCommodity(a04Model));
		}
		return listModel;
	}

}
