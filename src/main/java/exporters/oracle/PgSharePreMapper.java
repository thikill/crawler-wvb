package exporters.oracle;

import exporters.oracle.model.PgSharePre;
import exporters.oracle.model.PgSharePreExample;
import java.util.List;
import org.apache.ibatis.annotations.Param;

public interface PgSharePreMapper {
    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_SHARE_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    int countByExample(PgSharePreExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_SHARE_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    int deleteByExample(PgSharePreExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_SHARE_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    int insert(PgSharePre record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_SHARE_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    int insertSelective(PgSharePre record);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_SHARE_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    List<PgSharePre> selectByExample(PgSharePreExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_SHARE_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    int updateByExampleSelective(@Param("record") PgSharePre record, @Param("example") PgSharePreExample example);

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_SHARE_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    int updateByExample(@Param("record") PgSharePre record, @Param("example") PgSharePreExample example);
}