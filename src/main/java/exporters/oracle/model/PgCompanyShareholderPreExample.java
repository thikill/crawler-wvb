package exporters.oracle.model;

import java.util.ArrayList;
import java.util.List;

public class PgCompanyShareholderPreExample {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    protected String orderByClause;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    protected boolean distinct;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    protected List<Criteria> oredCriteria;

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public PgCompanyShareholderPreExample() {
        oredCriteria = new ArrayList<Criteria>();
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public void setOrderByClause(String orderByClause) {
        this.orderByClause = orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public String getOrderByClause() {
        return orderByClause;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public void setDistinct(boolean distinct) {
        this.distinct = distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public boolean isDistinct() {
        return distinct;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public List<Criteria> getOredCriteria() {
        return oredCriteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public void or(Criteria criteria) {
        oredCriteria.add(criteria);
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public Criteria or() {
        Criteria criteria = createCriteriaInternal();
        oredCriteria.add(criteria);
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public Criteria createCriteria() {
        Criteria criteria = createCriteriaInternal();
        if (oredCriteria.size() == 0) {
            oredCriteria.add(criteria);
        }
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    protected Criteria createCriteriaInternal() {
        Criteria criteria = new Criteria();
        return criteria;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public void clear() {
        oredCriteria.clear();
        orderByClause = null;
        distinct = false;
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    protected abstract static class GeneratedCriteria {
        protected List<Criterion> criteria;

        protected GeneratedCriteria() {
            super();
            criteria = new ArrayList<Criterion>();
        }

        public boolean isValid() {
            return criteria.size() > 0;
        }

        public List<Criterion> getAllCriteria() {
            return criteria;
        }

        public List<Criterion> getCriteria() {
            return criteria;
        }

        protected void addCriterion(String condition) {
            if (condition == null) {
                throw new RuntimeException("Value for condition cannot be null");
            }
            criteria.add(new Criterion(condition));
        }

        protected void addCriterion(String condition, Object value, String property) {
            if (value == null) {
                throw new RuntimeException("Value for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value));
        }

        protected void addCriterion(String condition, Object value1, Object value2, String property) {
            if (value1 == null || value2 == null) {
                throw new RuntimeException("Between values for " + property + " cannot be null");
            }
            criteria.add(new Criterion(condition, value1, value2));
        }

        public Criteria andRegistNameIsNull() {
            addCriterion("REGIST_NAME is null");
            return (Criteria) this;
        }

        public Criteria andRegistNameIsNotNull() {
            addCriterion("REGIST_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andRegistNameEqualTo(String value) {
            addCriterion("REGIST_NAME =", value, "registName");
            return (Criteria) this;
        }

        public Criteria andRegistNameNotEqualTo(String value) {
            addCriterion("REGIST_NAME <>", value, "registName");
            return (Criteria) this;
        }

        public Criteria andRegistNameGreaterThan(String value) {
            addCriterion("REGIST_NAME >", value, "registName");
            return (Criteria) this;
        }

        public Criteria andRegistNameGreaterThanOrEqualTo(String value) {
            addCriterion("REGIST_NAME >=", value, "registName");
            return (Criteria) this;
        }

        public Criteria andRegistNameLessThan(String value) {
            addCriterion("REGIST_NAME <", value, "registName");
            return (Criteria) this;
        }

        public Criteria andRegistNameLessThanOrEqualTo(String value) {
            addCriterion("REGIST_NAME <=", value, "registName");
            return (Criteria) this;
        }

        public Criteria andRegistNameLike(String value) {
            addCriterion("REGIST_NAME like", value, "registName");
            return (Criteria) this;
        }

        public Criteria andRegistNameNotLike(String value) {
            addCriterion("REGIST_NAME not like", value, "registName");
            return (Criteria) this;
        }

        public Criteria andRegistNameIn(List<String> values) {
            addCriterion("REGIST_NAME in", values, "registName");
            return (Criteria) this;
        }

        public Criteria andRegistNameNotIn(List<String> values) {
            addCriterion("REGIST_NAME not in", values, "registName");
            return (Criteria) this;
        }

        public Criteria andRegistNameBetween(String value1, String value2) {
            addCriterion("REGIST_NAME between", value1, value2, "registName");
            return (Criteria) this;
        }

        public Criteria andRegistNameNotBetween(String value1, String value2) {
            addCriterion("REGIST_NAME not between", value1, value2, "registName");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberIsNull() {
            addCriterion("COMPANY_NUMBER is null");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberIsNotNull() {
            addCriterion("COMPANY_NUMBER is not null");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberEqualTo(String value) {
            addCriterion("COMPANY_NUMBER =", value, "companyNumber");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberNotEqualTo(String value) {
            addCriterion("COMPANY_NUMBER <>", value, "companyNumber");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberGreaterThan(String value) {
            addCriterion("COMPANY_NUMBER >", value, "companyNumber");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberGreaterThanOrEqualTo(String value) {
            addCriterion("COMPANY_NUMBER >=", value, "companyNumber");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberLessThan(String value) {
            addCriterion("COMPANY_NUMBER <", value, "companyNumber");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberLessThanOrEqualTo(String value) {
            addCriterion("COMPANY_NUMBER <=", value, "companyNumber");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberLike(String value) {
            addCriterion("COMPANY_NUMBER like", value, "companyNumber");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberNotLike(String value) {
            addCriterion("COMPANY_NUMBER not like", value, "companyNumber");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberIn(List<String> values) {
            addCriterion("COMPANY_NUMBER in", values, "companyNumber");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberNotIn(List<String> values) {
            addCriterion("COMPANY_NUMBER not in", values, "companyNumber");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberBetween(String value1, String value2) {
            addCriterion("COMPANY_NUMBER between", value1, value2, "companyNumber");
            return (Criteria) this;
        }

        public Criteria andCompanyNumberNotBetween(String value1, String value2) {
            addCriterion("COMPANY_NUMBER not between", value1, value2, "companyNumber");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameIsNull() {
            addCriterion("FULL_LEGAL_NAME is null");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameIsNotNull() {
            addCriterion("FULL_LEGAL_NAME is not null");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameEqualTo(String value) {
            addCriterion("FULL_LEGAL_NAME =", value, "fullLegalName");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameNotEqualTo(String value) {
            addCriterion("FULL_LEGAL_NAME <>", value, "fullLegalName");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameGreaterThan(String value) {
            addCriterion("FULL_LEGAL_NAME >", value, "fullLegalName");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameGreaterThanOrEqualTo(String value) {
            addCriterion("FULL_LEGAL_NAME >=", value, "fullLegalName");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameLessThan(String value) {
            addCriterion("FULL_LEGAL_NAME <", value, "fullLegalName");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameLessThanOrEqualTo(String value) {
            addCriterion("FULL_LEGAL_NAME <=", value, "fullLegalName");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameLike(String value) {
            addCriterion("FULL_LEGAL_NAME like", value, "fullLegalName");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameNotLike(String value) {
            addCriterion("FULL_LEGAL_NAME not like", value, "fullLegalName");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameIn(List<String> values) {
            addCriterion("FULL_LEGAL_NAME in", values, "fullLegalName");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameNotIn(List<String> values) {
            addCriterion("FULL_LEGAL_NAME not in", values, "fullLegalName");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameBetween(String value1, String value2) {
            addCriterion("FULL_LEGAL_NAME between", value1, value2, "fullLegalName");
            return (Criteria) this;
        }

        public Criteria andFullLegalNameNotBetween(String value1, String value2) {
            addCriterion("FULL_LEGAL_NAME not between", value1, value2, "fullLegalName");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressIsNull() {
            addCriterion("OFFICE_ADDRESS is null");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressIsNotNull() {
            addCriterion("OFFICE_ADDRESS is not null");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressEqualTo(String value) {
            addCriterion("OFFICE_ADDRESS =", value, "officeAddress");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressNotEqualTo(String value) {
            addCriterion("OFFICE_ADDRESS <>", value, "officeAddress");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressGreaterThan(String value) {
            addCriterion("OFFICE_ADDRESS >", value, "officeAddress");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressGreaterThanOrEqualTo(String value) {
            addCriterion("OFFICE_ADDRESS >=", value, "officeAddress");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressLessThan(String value) {
            addCriterion("OFFICE_ADDRESS <", value, "officeAddress");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressLessThanOrEqualTo(String value) {
            addCriterion("OFFICE_ADDRESS <=", value, "officeAddress");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressLike(String value) {
            addCriterion("OFFICE_ADDRESS like", value, "officeAddress");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressNotLike(String value) {
            addCriterion("OFFICE_ADDRESS not like", value, "officeAddress");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressIn(List<String> values) {
            addCriterion("OFFICE_ADDRESS in", values, "officeAddress");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressNotIn(List<String> values) {
            addCriterion("OFFICE_ADDRESS not in", values, "officeAddress");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressBetween(String value1, String value2) {
            addCriterion("OFFICE_ADDRESS between", value1, value2, "officeAddress");
            return (Criteria) this;
        }

        public Criteria andOfficeAddressNotBetween(String value1, String value2) {
            addCriterion("OFFICE_ADDRESS not between", value1, value2, "officeAddress");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationIsNull() {
            addCriterion("PLACE_OF_INCORPORATION is null");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationIsNotNull() {
            addCriterion("PLACE_OF_INCORPORATION is not null");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationEqualTo(String value) {
            addCriterion("PLACE_OF_INCORPORATION =", value, "placeOfIncorporation");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationNotEqualTo(String value) {
            addCriterion("PLACE_OF_INCORPORATION <>", value, "placeOfIncorporation");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationGreaterThan(String value) {
            addCriterion("PLACE_OF_INCORPORATION >", value, "placeOfIncorporation");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationGreaterThanOrEqualTo(String value) {
            addCriterion("PLACE_OF_INCORPORATION >=", value, "placeOfIncorporation");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationLessThan(String value) {
            addCriterion("PLACE_OF_INCORPORATION <", value, "placeOfIncorporation");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationLessThanOrEqualTo(String value) {
            addCriterion("PLACE_OF_INCORPORATION <=", value, "placeOfIncorporation");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationLike(String value) {
            addCriterion("PLACE_OF_INCORPORATION like", value, "placeOfIncorporation");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationNotLike(String value) {
            addCriterion("PLACE_OF_INCORPORATION not like", value, "placeOfIncorporation");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationIn(List<String> values) {
            addCriterion("PLACE_OF_INCORPORATION in", values, "placeOfIncorporation");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationNotIn(List<String> values) {
            addCriterion("PLACE_OF_INCORPORATION not in", values, "placeOfIncorporation");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationBetween(String value1, String value2) {
            addCriterion("PLACE_OF_INCORPORATION between", value1, value2, "placeOfIncorporation");
            return (Criteria) this;
        }

        public Criteria andPlaceOfIncorporationNotBetween(String value1, String value2) {
            addCriterion("PLACE_OF_INCORPORATION not between", value1, value2, "placeOfIncorporation");
            return (Criteria) this;
        }

        public Criteria andConsentedIsNull() {
            addCriterion("CONSENTED is null");
            return (Criteria) this;
        }

        public Criteria andConsentedIsNotNull() {
            addCriterion("CONSENTED is not null");
            return (Criteria) this;
        }

        public Criteria andConsentedEqualTo(String value) {
            addCriterion("CONSENTED =", value, "consented");
            return (Criteria) this;
        }

        public Criteria andConsentedNotEqualTo(String value) {
            addCriterion("CONSENTED <>", value, "consented");
            return (Criteria) this;
        }

        public Criteria andConsentedGreaterThan(String value) {
            addCriterion("CONSENTED >", value, "consented");
            return (Criteria) this;
        }

        public Criteria andConsentedGreaterThanOrEqualTo(String value) {
            addCriterion("CONSENTED >=", value, "consented");
            return (Criteria) this;
        }

        public Criteria andConsentedLessThan(String value) {
            addCriterion("CONSENTED <", value, "consented");
            return (Criteria) this;
        }

        public Criteria andConsentedLessThanOrEqualTo(String value) {
            addCriterion("CONSENTED <=", value, "consented");
            return (Criteria) this;
        }

        public Criteria andConsentedLike(String value) {
            addCriterion("CONSENTED like", value, "consented");
            return (Criteria) this;
        }

        public Criteria andConsentedNotLike(String value) {
            addCriterion("CONSENTED not like", value, "consented");
            return (Criteria) this;
        }

        public Criteria andConsentedIn(List<String> values) {
            addCriterion("CONSENTED in", values, "consented");
            return (Criteria) this;
        }

        public Criteria andConsentedNotIn(List<String> values) {
            addCriterion("CONSENTED not in", values, "consented");
            return (Criteria) this;
        }

        public Criteria andConsentedBetween(String value1, String value2) {
            addCriterion("CONSENTED between", value1, value2, "consented");
            return (Criteria) this;
        }

        public Criteria andConsentedNotBetween(String value1, String value2) {
            addCriterion("CONSENTED not between", value1, value2, "consented");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateIsNull() {
            addCriterion("APPOINTMENT_DATE is null");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateIsNotNull() {
            addCriterion("APPOINTMENT_DATE is not null");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateEqualTo(String value) {
            addCriterion("APPOINTMENT_DATE =", value, "appointmentDate");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateNotEqualTo(String value) {
            addCriterion("APPOINTMENT_DATE <>", value, "appointmentDate");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateGreaterThan(String value) {
            addCriterion("APPOINTMENT_DATE >", value, "appointmentDate");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateGreaterThanOrEqualTo(String value) {
            addCriterion("APPOINTMENT_DATE >=", value, "appointmentDate");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateLessThan(String value) {
            addCriterion("APPOINTMENT_DATE <", value, "appointmentDate");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateLessThanOrEqualTo(String value) {
            addCriterion("APPOINTMENT_DATE <=", value, "appointmentDate");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateLike(String value) {
            addCriterion("APPOINTMENT_DATE like", value, "appointmentDate");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateNotLike(String value) {
            addCriterion("APPOINTMENT_DATE not like", value, "appointmentDate");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateIn(List<String> values) {
            addCriterion("APPOINTMENT_DATE in", values, "appointmentDate");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateNotIn(List<String> values) {
            addCriterion("APPOINTMENT_DATE not in", values, "appointmentDate");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateBetween(String value1, String value2) {
            addCriterion("APPOINTMENT_DATE between", value1, value2, "appointmentDate");
            return (Criteria) this;
        }

        public Criteria andAppointmentDateNotBetween(String value1, String value2) {
            addCriterion("APPOINTMENT_DATE not between", value1, value2, "appointmentDate");
            return (Criteria) this;
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated do_not_delete_during_merge Tue Jul 21 10:35:55 ICT 2015
     */
    public static class Criteria extends GeneratedCriteria {

        protected Criteria() {
            super();
        }
    }

    /**
     * This class was generated by MyBatis Generator.
     * This class corresponds to the database table PG_COMPANY_SHAREHOLDER_PRE
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public static class Criterion {
        private String condition;

        private Object value;

        private Object secondValue;

        private boolean noValue;

        private boolean singleValue;

        private boolean betweenValue;

        private boolean listValue;

        private String typeHandler;

        public String getCondition() {
            return condition;
        }

        public Object getValue() {
            return value;
        }

        public Object getSecondValue() {
            return secondValue;
        }

        public boolean isNoValue() {
            return noValue;
        }

        public boolean isSingleValue() {
            return singleValue;
        }

        public boolean isBetweenValue() {
            return betweenValue;
        }

        public boolean isListValue() {
            return listValue;
        }

        public String getTypeHandler() {
            return typeHandler;
        }

        protected Criterion(String condition) {
            super();
            this.condition = condition;
            this.typeHandler = null;
            this.noValue = true;
        }

        protected Criterion(String condition, Object value, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.typeHandler = typeHandler;
            if (value instanceof List<?>) {
                this.listValue = true;
            } else {
                this.singleValue = true;
            }
        }

        protected Criterion(String condition, Object value) {
            this(condition, value, null);
        }

        protected Criterion(String condition, Object value, Object secondValue, String typeHandler) {
            super();
            this.condition = condition;
            this.value = value;
            this.secondValue = secondValue;
            this.typeHandler = typeHandler;
            this.betweenValue = true;
        }

        protected Criterion(String condition, Object value, Object secondValue) {
            this(condition, value, secondValue, null);
        }
    }
}