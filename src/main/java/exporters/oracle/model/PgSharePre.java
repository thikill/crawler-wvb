package exporters.oracle.model;

public class PgSharePre {
    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column PG_SHARE_PRE.REGIST_NAME
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    private String registName;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column PG_SHARE_PRE.NUMBER_OF_SHARES
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    private String numberOfShares;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column PG_SHARE_PRE.NAME
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    private String name;

    /**
     * This field was generated by MyBatis Generator.
     * This field corresponds to the database column PG_SHARE_PRE.COMPANY_NUMBER
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    private String companyNumber;

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column PG_SHARE_PRE.REGIST_NAME
     *
     * @return the value of PG_SHARE_PRE.REGIST_NAME
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public String getRegistName() {
        return registName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column PG_SHARE_PRE.REGIST_NAME
     *
     * @param registName the value for PG_SHARE_PRE.REGIST_NAME
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public void setRegistName(String registName) {
        this.registName = registName;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column PG_SHARE_PRE.NUMBER_OF_SHARES
     *
     * @return the value of PG_SHARE_PRE.NUMBER_OF_SHARES
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public String getNumberOfShares() {
        return numberOfShares;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column PG_SHARE_PRE.NUMBER_OF_SHARES
     *
     * @param numberOfShares the value for PG_SHARE_PRE.NUMBER_OF_SHARES
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public void setNumberOfShares(String numberOfShares) {
        this.numberOfShares = numberOfShares;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column PG_SHARE_PRE.NAME
     *
     * @return the value of PG_SHARE_PRE.NAME
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public String getName() {
        return name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column PG_SHARE_PRE.NAME
     *
     * @param name the value for PG_SHARE_PRE.NAME
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public void setName(String name) {
        this.name = name;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method returns the value of the database column PG_SHARE_PRE.COMPANY_NUMBER
     *
     * @return the value of PG_SHARE_PRE.COMPANY_NUMBER
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public String getCompanyNumber() {
        return companyNumber;
    }

    /**
     * This method was generated by MyBatis Generator.
     * This method sets the value of the database column PG_SHARE_PRE.COMPANY_NUMBER
     *
     * @param companyNumber the value for PG_SHARE_PRE.COMPANY_NUMBER
     *
     * @mbggenerated Tue Jul 21 10:35:55 ICT 2015
     */
    public void setCompanyNumber(String companyNumber) {
        this.companyNumber = companyNumber;
    }
}