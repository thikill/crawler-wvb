package exporters.oracle.model;
import java.util.List;

import exporters.oracle.model.*;
public class PNGCompany {
	public List<PgAddressPre> getPgAddressPre() {
		return pgAddressPre;
	}
	public void setPgAddressPre(List<PgAddressPre> pgAddressPre) {
		this.pgAddressPre = pgAddressPre;
	}
	public List<PgCompanyShareholderPre> getPgCompanyShareholderPre() {
		return pgCompanyShareholderPre;
	}
	public void setPgCompanyShareholderPre(
			List<PgCompanyShareholderPre> pgCompanyShareholderPre) {
		this.pgCompanyShareholderPre = pgCompanyShareholderPre;
	}
	public PgDetailPre getPgDetailPre() {
		return pgDetailPre;
	}
	public void setPgDetailPre(PgDetailPre pgDetailPre) {
		this.pgDetailPre = pgDetailPre;
	}
	public List<PgPersonPre> getPgPersonPre() {
		return pgPersonPre;
	}
	public void setPgPersonPre(List<PgPersonPre> pgPersonPre) {
		this.pgPersonPre = pgPersonPre;
	}
	public List<PgSharePre> getPgSharePre() {
		return pgSharePre;
	}
	public void setPgSharePre(List<PgSharePre> pgSharePre) {
		this.pgSharePre = pgSharePre;
	}
	private List<PgAddressPre>  pgAddressPre;
	private List<PgCompanyShareholderPre> pgCompanyShareholderPre;
	private PgDetailPre  pgDetailPre;
	private List<PgPersonPre> pgPersonPre;
	private List<PgSharePre> pgSharePre;
}
