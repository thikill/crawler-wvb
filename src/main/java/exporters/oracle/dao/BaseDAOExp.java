package exporters.oracle.dao;

import java.io.InputStream;

import org.apache.ibatis.io.Resources;
import org.apache.ibatis.session.SqlSession;
import org.apache.ibatis.session.SqlSessionFactory;
import org.apache.ibatis.session.SqlSessionFactoryBuilder;

public class BaseDAOExp {
	private static final String conf = "/mybatis-config.xml";
	static final SqlSessionFactory FACTORY;
	SqlSession session;
	static {
		try {
			//InputStream inputStream = Resources.getResourceAsStream(conf);
			InputStream is = BaseDAOExp.class.getResourceAsStream( conf );
		
			FACTORY = new SqlSessionFactoryBuilder().build(is);
		} catch (Exception e) {
			throw new RuntimeException("Fatal Error.  Cause: " + e, e);
		}
	}

	public static SqlSessionFactory getSqlSessionFactory() {
		return FACTORY;
	}
}
