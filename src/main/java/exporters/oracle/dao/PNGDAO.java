package exporters.oracle.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dao.A02Dao;
import exporters.oracle.model.OandaCurrencyRates;
import exporters.oracle.model.PNGCompany;
import exporters.oracle.model.PgAddressPre;
import exporters.oracle.model.PgCompanyShareholderPre;
import exporters.oracle.model.PgDetailPre;
import exporters.oracle.model.PgPersonPre;
import exporters.oracle.model.PgSharePre;

public class PNGDAO extends BaseDAOExp {
	final static Logger logger = LogManager.getLogger(PNGDAO.class);

	public void insertPgAddressPre(PgAddressPre pgAddressPre) {
		session = FACTORY.openSession();
		try {
			session.insert("exporters.oracle.PgAddressPreMapper.insert",
					pgAddressPre);
			session.commit();
		} catch (Exception e) {
			logger.warn(e.getMessage());
		} finally {
			session.close();
		}
	}

	public void insertCompany(PNGCompany pngCompany) throws  Exception{
		session = FACTORY.openSession();
		try {

			List<PgAddressPre> pgAddressPre = pngCompany.getPgAddressPre();
			List<PgCompanyShareholderPre> pgCompanyShareholderPre = pngCompany
					.getPgCompanyShareholderPre();
			PgDetailPre pgDetailPre = pngCompany.getPgDetailPre();
			List<PgPersonPre> pgPersonPre = pngCompany.getPgPersonPre();
			List<PgSharePre> pgSharePre = pngCompany.getPgSharePre();

			session.insert("exporters.oracle.PgDetailPreMapper.insert", pgDetailPre);

			for (PgAddressPre item : pgAddressPre) {
				session.insert("exporters.oracle.PgAddressPreMapper.insert",
						item);
			}

			for (PgCompanyShareholderPre item : pgCompanyShareholderPre) {
				session.insert(
						"exporters.oracle.PgCompanyShareholderPreMapper.insert", item);
			}
			for (PgPersonPre item : pgPersonPre) {
				session.insert("exporters.oracle.PgPersonPreMapper.insert", item);
			}

			for (PgSharePre item : pgSharePre) {
				session.insert("exporters.oracle.PgSharePreMapper.insert", item);
			}

			session.commit();
		} catch (Exception e) {
			
			logger.warn(e.getMessage());
			throw e;
		} finally {
			session.close();
		}
	}

	public static void main(String[] args) {
		PNGDAO dao = new PNGDAO();
		PgAddressPre pgAddressPre = new PgAddressPre();
		pgAddressPre.setAddress("address");
		pgAddressPre.setAddressType("POSTAL");
		pgAddressPre.setRegistName("name");
		pgAddressPre.setStartDate("xxx");

		dao.insertPgAddressPre(pgAddressPre);
	}
}
