package exporters.oracle.dao;

import java.math.BigDecimal;
import java.sql.SQLException;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import dao.A02Dao;
import exporters.oracle.model.OandaCurrencyRates;

public class OandaCurrencyRatesDAOExp extends BaseDAOExp {
	final static Logger logger = LogManager
			.getLogger(OandaCurrencyRatesDAOExp.class);

	public void insertCCYRate(OandaCurrencyRates ccyRate) {
		session = FACTORY.openSession();
		try {
			session.insert("exporters.oracle.OandaCurrencyRatesMapper.insert",
					ccyRate);
			session.commit();
		} catch (Exception e) {
			logger.warn(e.getMessage());
		} finally {
			session.close();
		}
	}

	public void insertListCCYRate(List<OandaCurrencyRates> listCCYRate) {
		session = FACTORY.openSession();
		try {
			session.insert(
					"exporters.oracle.OandaCurrencyRatesMapper.insertList",
					listCCYRate);
			session.commit();
		} catch (Exception e) {
			logger.warn(e.getMessage());
		} finally {
			session.close();
		}
	}

	public static void main(String[] args) {
		OandaCurrencyRatesDAOExp dao = new OandaCurrencyRatesDAOExp();
		OandaCurrencyRates ccyRate = new OandaCurrencyRates();
		ccyRate.setIsoCurrencyCode("VND");
		ccyRate.setRate(new BigDecimal(1));
		ccyRate.setRateDate(new Date());
		dao.insertCCYRate(ccyRate);
	}
}
