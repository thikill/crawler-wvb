package exporters.csv;

import java.io.File;
import java.io.FileWriter;
import java.util.List;

import model.A04Model;

import org.supercsv.cellprocessor.FmtBool;
import org.supercsv.cellprocessor.FmtDate;
import org.supercsv.cellprocessor.Optional;
import org.supercsv.cellprocessor.constraint.LMinMax;
import org.supercsv.cellprocessor.constraint.NotNull;
import org.supercsv.cellprocessor.constraint.UniqueHashCode;
import org.supercsv.cellprocessor.ift.CellProcessor;
import org.supercsv.io.CsvBeanWriter;
import org.supercsv.io.ICsvBeanWriter;
import org.supercsv.prefs.CsvPreference;

import exporters.converter.Converter;
import utils.FileUtils;

public class CommodityExporter {
	private static Converter converter;

	private static CellProcessor[] getProcessors() {

		final CellProcessor[] processors = new CellProcessor[] { new NotNull(), // firstName
				new NotNull(), // lastName
				new NotNull(), // mailingAddress
				new NotNull(), // favouriteQuote
				new NotNull(), // favouriteQuote
				new NotNull(), // favouriteQuote
		};

		return processors;
	}

	public static void exportToCSV(List<A04Model> listData) throws Exception {
		converter = new Converter();
		List<CommodityModel> listModel = converter
				.convertListA04Model(listData);
		ICsvBeanWriter beanWriter = null;
		try {
			File targetFile = new File(FileUtils.getJarDir() + "/export"
					+ "/Commodity.csv");
			File parent = targetFile.getParentFile();
			if(!parent.exists() && !parent.mkdirs()){
			    throw new IllegalStateException("Couldn't create dir: " + parent);
			}
			beanWriter = new CsvBeanWriter(new FileWriter(targetFile),
					CsvPreference.STANDARD_PREFERENCE);

			// the header elements are used to map the bean values to each
			// column (names must match)
			final String[] header = new String[] { "Name", "Code", "ccyCode","Unit",
					"Time", "Price" };
			final CellProcessor[] processors = getProcessors();

			// write the header
			beanWriter.writeHeader(header);

			// write the beans
			for (final CommodityModel model : listModel) {
				beanWriter.write(model, header, processors);
			}

		} finally {
			if (beanWriter != null) {
				beanWriter.close();
			}
		}

	}
}
