package model.B02;

import java.util.List;

import model.ModelBase;

/**
 * @author thi
 *
 */
public class Product extends ModelBase {
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getUrl() {
		return url;
	}
	public void setUrl(String url) {
		this.url = url;
	}
	public List<Image> getImages() {
		return images;
	}
	public void setImages(List<Image> images) {
		this.images = images;
	}
	public List<CustomizationChoices> getChoices() {
		return choices;
	}
	public void setChoices(List<CustomizationChoices> choices) {
		this.choices = choices;
	}
	private String id;
	public String getId() {
		return id;
	}
	public void setId(String id) {
		this.id = id;
	}
	
	private String title;
	public String getImageURL() {
		return imageURL;
	}
	public void setImageURL(String imageURL) {
		this.imageURL = imageURL;
	}
	private String category;
	public String getCategory() {
		return category;
	}
	public void setCategory(String category) {
		this.category = category;
	}
	private String imageURL;
	private String url;
	private List<Image> images;
	private List<CustomizationChoices> choices;
}
