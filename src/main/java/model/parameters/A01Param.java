package model.parameters;

import com.beust.jcommander.Parameter;

public class A01Param {
	public String getStartDate() {
		return startDate;
	}

	public void setStartDate(String startDate) {
		this.startDate = startDate;
	}

	public String getEndDate() {
		return endDate;
	}

	public void setEndDate(String endDate) {
		this.endDate = endDate;
	}

	@Parameter(names = "-startDate", description = "start date")
	private String startDate;
	@Parameter(names = "-endDate", description = "end date")
	private String endDate;
}
