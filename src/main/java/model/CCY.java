package model;

import com.j256.ormlite.field.DatabaseField;

public class CCY  {

	@DatabaseField(generatedId = true)
	private int id;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getValue() {
		return value;
	}

	public void setValue(String value) {
		this.value = value;
	}

	public String getDisplay() {
		return display;
	}

	public void setDisplay(String display) {
		this.display = display;
	}

	@DatabaseField
	private String value;
	@DatabaseField
	private String display;
	@DatabaseField(defaultValue="0")
	private int type;

	public int getType() {
		return type;
	}

	public void setType(int type) {
		this.type = type;
	}
	
	
	@Override
	public boolean equals(Object obj) {
		CCY dest = (CCY) obj;
		if (dest.value.equals(this.value))
			return true;
		return false;

	}


	
	public static void main(String[] args) {
		CCY a = new CCY();
		a.setValue("VNDfe");
		CCY b = new CCY();
		b.setValue("VND");
		System.out.println(a.equals(b));
	}
	
}




