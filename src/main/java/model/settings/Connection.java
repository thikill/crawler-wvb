package model.settings;

import java.util.Scanner;

public class Connection {
	// second
	private int timeout;

	public int getTimeout() {
		return timeout;
	}

	public void setTimeout(int timeout) {
		this.timeout = timeout;
	}

	public int getDelay() {
		return delay;
	}

	public void setDelay(int delay) {
		this.delay = delay;
	}

	public int getTargetWaitTime() {
		return targetWaitTime;
	}

	public void setTargetWaitTime(int targetWaitTime) {
		this.targetWaitTime = targetWaitTime;
	}

	public boolean isEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public int getTimes() {
		return times;
	}

	public void setTimes(int times) {
		this.times = times;
	}

	public int[] getHttpCodes() {
		return httpCodes;
	}

	public void setHttpCodes(int[] httpCodes) {
		this.httpCodes = httpCodes;
	}

	// The wait time after doing every actions. milisecond
	private int delay;
	// If program can't find the target, it will wait some time, and try again.
	private int targetWaitTime;

	private boolean enabled;
	private int times;
	private int[] httpCodes;
	private int maxFailed;

	public int getMaxFailed() {
		return maxFailed;
	}

	public void setMaxFailed(int maxFailed) {
		this.maxFailed = maxFailed;
	}

}
