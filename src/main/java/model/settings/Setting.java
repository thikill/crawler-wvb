package model.settings;

public class Setting {
	public Concurrent getConcurrent() {
		return concurrent;
	}
	public void setConcurrent(Concurrent concurrent) {
		this.concurrent = concurrent;
	}
	public Connection getConnection() {
		return connection;
	}
	public void setConnection(Connection connection) {
		this.connection = connection;
	}
	public RequestHeader getRequestHeader() {
		return requestHeader;
	}
	public void setRequestHeader(RequestHeader requestHeader) {
		this.requestHeader = requestHeader;
	}
	private Concurrent concurrent;
	private Connection connection;
	private RequestHeader requestHeader;
	private boolean dnsCacheEnabled;
	public boolean isDnsCacheEnabled() {
		return dnsCacheEnabled;
	}
	public void setDnsCacheEnabled(boolean dnsCacheEnabled) {
		this.dnsCacheEnabled = dnsCacheEnabled;
	}
	private SpiderInfo spider;
	public SpiderInfo getSpider() {
		return spider;
	}
	public void setSpider(SpiderInfo spider) {
		this.spider = spider;
	} 
	
}
