package model;

import java.lang.reflect.Field;
import java.util.Iterator;

public class ModelBase {
	public String toString() {
		String result = "{";
		Class c = this.getClass();
		Field[] fields = c.getDeclaredFields();
		int count = 0;
		int length = fields.length;
		for (Field field : fields) {
			field.setAccessible(true);
			String name = field.getName();
			String value = "";
			try {
				Object objectValue = field.get(this);
				value =objectValue==null?value: objectValue.toString();
			} catch (IllegalArgumentException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			} catch (IllegalAccessException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			count++;
			if (count<length) {
				result = result + "\""+name +"\":\"" + value +"\" , ";
			} else {
				result = result + "\""+name +"\":\"" + value +"\" ";
			}
		}
		result = result + "}";
		return result;
	}
}
