/**
 * 
 */
package spiders;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Array;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.TimeUnit;

import model.A03KeyValue;
import model.A03Model;
import model.parameters.CommandParameters;

import org.apache.commons.io.FileUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.remote.server.DriverFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import common.RequestConstants;
import core.selenium.factory.WebDriverController;
import core.selenium.factory.WebDriverFactory;
import dao.A03Dao;
import exporters.oracle.dao.PNGDAO;
import exporters.oracle.model.PNGCompany;
import exporters.oracle.model.PgAddressPre;
import exporters.oracle.model.PgCompanyShareholderPre;
import exporters.oracle.model.PgDetailPre;
import exporters.oracle.model.PgPersonPre;
import exporters.oracle.model.PgSharePre;

/**
 * @author thi
 *
 */
public class A03 {

	public static final int MAX_QUEUE_SIZE = 100000;
	public static BlockingQueue<A03Model> taskQueue = new ArrayBlockingQueue<A03Model>(
			MAX_QUEUE_SIZE);
	static Logger logger = LogManager.getLogger(A03.class.getName());

	public static void getTaskQueue(List<A03Model> listData) {
		for (A03Model model : listData) {
			taskQueue.add(model);
		}

	}

	public static void mkDir() {
		File outDir = new File("out");
		if (outDir.exists() == false) {
			outDir.mkdir();
		}
	}

	public static void writeFile(String id, String tab) {
		try {
			File fContent = new File("out" + File.separator + id + ".html");
			if (fContent.exists() == false) {

				fContent.createNewFile();

			}
			FileUtils.writeStringToFile(fContent, tab, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void cleanFile(String id) {
		try {
			File fContent = new File("out" + File.separator + id + ".html");
			if (fContent.exists() == true) {
				fContent.delete();

			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void writeFail(String id) {
		try {
			File fContent = new File("out" + File.separator + "fail" + ".txt");
			if (fContent.exists() == false) {

				fContent.createNewFile();

			}
			FileUtils.writeStringToFile(fContent, "\r\n" + id, true);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	public static void doSomething(String id) {
		WebDriver driver = null;
		try {
			cleanFile(id);
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(
					PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
					utils.FileUtils.getJarDir() + File.separator + "phantomjs");

			/*
			 * capabilities.setCapability(
			 * PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
			 * "C:/phantomjs.exe");
			 */

			// System.setProperty("webdriver.ie.driver",
			// "C:/IEDriverServer.exe");
			// System.setProperty("webdriver.chrome.driver",
			// "C:/chromedriver.exe");
			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability("takesScreenshot", false);
			capabilities.setBrowserName(BrowserType.PHANTOMJS);
			capabilities.setPlatform(Platform.ANY);
			capabilities.setCapability(
					PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {
							"--web-security=false", "--ssl-protocol=any",
							"--ignore-ssl-errors=true", "--load-images=no",
							"--disk-cache=true", "--output-encoding=utf-8" });

			 driver = WebDriverFactory.getDriver(capabilities);
			//driver = WebDriverFactory.getDriver(DesiredCapabilities.firefox());

			driver.manage().timeouts().implicitlyWait(180, TimeUnit.SECONDS);
			long startTime = System.currentTimeMillis();

			// WebDriver driver =
			// WebDriverFactory.getDriver(DesiredCapabilities.internetExplorer());

			driver.get("http://www.ipa.gov.pg/pngcompanies/service/createVersion.html?service=brViewCompany&domainName=Item&versionIdentifier="
					+ id);

			WebDriverController controller = new WebDriverController(driver);
			controller.waitClickable(By.id("tabaW105_detailsTab"));

			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab2 = driver.findElement(By
					.cssSelector("a[id=tabaW105_addressesTab]"));
			tab2.click();
			controller.waitClickable(By
					.cssSelector("a[id=tabaW105_directorsTab]"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab3 = driver.findElement(By
					.cssSelector("a[id=tabaW105_directorsTab]"));
			tab3.click();

			controller.waitClickable(By.id("tabaW105_shareholdingTab"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab4 = driver.findElement(By
					.cssSelector("a[id=tabaW105_shareholdingTab]"));
			tab4.click();
			controller.waitClickable(By.id("tabaW105_bundleAllocationTab"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab5 = driver.findElement(By
					.cssSelector("a[id=tabaW105_bundleAllocationTab]"));
			tab5.click();
			controller.waitClickable(By.id("tabaW105_secretariesTab"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab6 = driver.findElement(By
					.cssSelector("a[id=tabaW105_secretariesTab]"));
			tab6.click();
			controller.waitClickable(By.id("tabaW105_filings"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab7 = driver.findElement(By
					.cssSelector("a[id=tabaW105_filings]"));
			tab7.click();
			controller.waitClickable(By.id("tabaW105_addressesTab"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			A03Dao.UpdateStatus(id, 1);
			System.out.println("id =====" + id);
			long endTime = System.currentTimeMillis();
			System.out.println("time =" + (endTime - startTime));
			// driver.quit();
		} catch (Exception e) {
			writeFail(id);
			File fContent = new File("out" + File.separator + id + ".html");
			if (fContent.exists() == true) {
				fContent.delete();
			}
			logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			driver.close();

		}
	}

	public static void doSomething3(String id) {
		WebDriver driver = null;
		try {
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(
					PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
					utils.FileUtils.getJarDir() + File.separator + "phantomjs");
			// System.setProperty("webdriver.ie.driver",
			// "C:/IEDriverServer.exe");
			// System.setProperty("webdriver.chrome.driver",
			// "C:/chromedriver.exe");
			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability("takesScreenshot", false);
			capabilities.setBrowserName(BrowserType.PHANTOMJS);
			capabilities.setPlatform(Platform.ANY);
			capabilities.setCapability(
					PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {
							"--web-security=false", "--ssl-protocol=any",
							"--ignore-ssl-errors=true", "--load-images=no",
							"--disk-cache=true", "--output-encoding=utf-8" });

			driver = WebDriverFactory.getDriver(capabilities);
			// driver =
			// WebDriverFactory.getDriver(DesiredCapabilities.firefox());
			driver.manage().timeouts().implicitlyWait(180, TimeUnit.SECONDS);
			long startTime = System.currentTimeMillis();

			// WebDriver driver =
			// WebDriverFactory.getDriver(DesiredCapabilities.internetExplorer());
			driver.get("http://www.ipa.gov.pg/pngcompanies/service/createVersion.html?service=brViewCompany&domainName=Item&versionIdentifier="
					+ id + "&version=1");
			WebDriverController controller = new WebDriverController(driver);

			controller.waitClickable(By.id("tabaW103_detailsTab"));

			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab2 = driver.findElement(By
					.cssSelector("a[id=tabaW103_addressesTab]"));
			tab2.click();
			controller.waitClickable(By
					.cssSelector("a[id=tabaW103_acceptSopTab]"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab3 = driver.findElement(By
					.cssSelector("a[id=tabaW103_acceptSopTab]"));
			tab3.click();

			controller.waitClickable(By.id("tabaW103_directorsTab"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab4 = driver.findElement(By
					.cssSelector("a[id=tabaW103_directorsTab]"));
			tab4.click();
			controller.waitClickable(By.id("tabaW103_filings"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab5 = driver.findElement(By
					.cssSelector("a[id=tabaW103_filings]"));
			tab5.click();
			controller.waitClickable(By.id("tabaW103_filings"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			A03Dao.UpdateStatus(id, 1);
			System.out.println("id =====" + id);
			long endTime = System.currentTimeMillis();
			System.out.println("time =" + (endTime - startTime));
			// driver.quit();
		} catch (Exception e) {
			writeFail(id);
			File fContent = new File("out" + File.separator + id + ".html");
			if (fContent.exists() == true) {

				fContent.delete();

			}
			logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			driver.close();

		}
	}

	public static void doSomethingVersion(String id) {
		WebDriver driver = null;
		try {
			cleanFile(id);
			DesiredCapabilities capabilities = new DesiredCapabilities();
			capabilities.setCapability(
					PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
					utils.FileUtils.getJarDir() + File.separator + "phantomjs");
			// System.setProperty("webdriver.ie.driver",
			// "C:/IEDriverServer.exe");
			// System.setProperty("webdriver.chrome.driver",
			// "C:/chromedriver.exe");
			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability("takesScreenshot", false);
			capabilities.setBrowserName(BrowserType.PHANTOMJS);
			capabilities.setPlatform(Platform.ANY);
			capabilities.setCapability(
					PhantomJSDriverService.PHANTOMJS_CLI_ARGS, new String[] {
							"--web-security=false", "--ssl-protocol=any",
							"--ignore-ssl-errors=true", "--load-images=no",
							"--disk-cache=true", "--output-encoding=utf-8" });

			driver = WebDriverFactory.getDriver(capabilities);
			driver.manage().timeouts().implicitlyWait(180, TimeUnit.SECONDS);
			long startTime = System.currentTimeMillis();

			// WebDriver driver =
			// WebDriverFactory.getDriver(DesiredCapabilities.internetExplorer());
			driver.get("http://www.ipa.gov.pg/pngcompanies/service/createVersion.html?service=brViewCompany&domainName=Item&versionIdentifier="
					+ id + "&version=1");
			WebDriverController controller = new WebDriverController(driver);
			controller.waitClickable(By.id("tabaW105_detailsTab"));

			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab2 = driver.findElement(By
					.cssSelector("a[id=tabaW105_addressesTab]"));
			tab2.click();
			controller.waitClickable(By
					.cssSelector("a[id=tabaW105_directorsTab]"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab3 = driver.findElement(By
					.cssSelector("a[id=tabaW105_directorsTab]"));
			tab3.click();

			controller.waitClickable(By.id("tabaW105_shareholdingTab"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab4 = driver.findElement(By
					.cssSelector("a[id=tabaW105_shareholdingTab]"));
			tab4.click();
			controller.waitClickable(By.id("tabaW105_bundleAllocationTab"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab5 = driver.findElement(By
					.cssSelector("a[id=tabaW105_bundleAllocationTab]"));
			tab5.click();
			controller.waitClickable(By.id("tabaW105_secretariesTab"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab6 = driver.findElement(By
					.cssSelector("a[id=tabaW105_secretariesTab]"));
			tab6.click();
			controller.waitClickable(By.id("tabaW105_filings"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			WebElement tab7 = driver.findElement(By
					.cssSelector("a[id=tabaW105_filings]"));
			tab7.click();
			controller.waitClickable(By.id("tabaW105_filings"));
			writeFile(
					id,
					driver.findElement(
							By.cssSelector("span[id=AsyncWrapperW1]"))
							.getAttribute("innerHTML").toString());

			A03Dao.UpdateStatus(id, 1);
			System.out.println("id =====" + id);
			long endTime = System.currentTimeMillis();
			System.out.println("time =" + (endTime - startTime));
			// driver.quit();
		} catch (Exception e) {
			writeFail(id);
			File fContent = new File("out" + File.separator + id + ".html");
			if (fContent.exists() == true) {

				fContent.delete();

			}
			logger.error(e.getMessage());
			e.printStackTrace();
		} finally {
			driver.close();

		}
	}

	public static void getDataByCompany() {

		Thread t = new Thread(new Runnable() {

			public void run() {
				while (true) {
					if (taskQueue.size() > 0) {
						A03Model model = taskQueue.poll();
						doSomething(model.companyId);
					} else {
						break;
					}
				}
			}
		});
		t.start();

	}

	public static void getDataByCompanySecond() {

		Thread t = new Thread(new Runnable() {

			public void run() {
				while (true) {
					if (taskQueue.size() > 0) {
						A03Model model = taskQueue.poll();
						doSomething3(model.companyId);
					} else {
						break;
					}
				}
			}
		});
		t.start();

	}

	public static void getDataByCompanyVersion() {

		Thread t = new Thread(new Runnable() {

			public void run() {
				while (true) {
					if (taskQueue.size() > 0) {
						A03Model model = taskQueue.poll();
						doSomethingVersion(model.companyId);
					} else {
						break;
					}
				}
			}
		});
		t.start();

	}

	public static void getData(CommandParameters commandParams) {

		int request = commandParams.getRequest();
		switch (request) {
		case RequestConstants.RUN_PNG:
			crawl(commandParams);
			break;
		case RequestConstants.RUN_PNG_3:
			crawl3(commandParams);
			break;
		case RequestConstants.RUN_PNG_VERSION:
			crawlVersion(commandParams);
			break;
		case RequestConstants.RUN_PNG_PARSE:
			extract(commandParams);
			break;

		}

	}

	public static void extract(CommandParameters commandParams) {
		try {

			List<A03Model> listData = A03Dao.getListCompanyForExtract(
					commandParams.getFrom(), commandParams.getTo());
			getTaskQueue(listData);
			logger.debug("queue size == " + taskQueue.size());
			int thread = commandParams.getThread();
			for (int i = 0; i < thread; i++) {
				extractDataCompany();
			}
		} catch (SQLException e) {
			logger.error(e);
		}

	}

	public static void extractDataCompany() {

		Thread t = new Thread(new Runnable() {

			public void run() {
				while (true) {
					if (taskQueue.size() > 0) {
						A03Model model = taskQueue.poll();
						doExtract(model.companyId);
					} else {
						break;
					}
				}
			}
		});
		t.start();

	}

	public static void doExtract(String id) {
		logger.debug("vao  doExtract :" + id);

		try {
			File fContent = new File("out" + File.separator + id + ".html");
			Document document = Jsoup.parse(fContent, "UTF-8");
			Elements listE = document.select("span[class=appLabelText]");
			// Elements listE = document.select("div[class=appAttrValue]");
			ArrayList<A03KeyValue> listData = new ArrayList<A03KeyValue>();

			for (Element element : listE) {

				String key = element.text();

				Elements eSiblling = element.parent().parent()
						.siblingElements();
				if ("appAttrValue".equals(eSiblling.first().className()) == false)
					continue;
				String value = eSiblling.first().text();
				A03KeyValue objInfo = new A03KeyValue();

				if (key != null && key.equals("") == false) {
					objInfo.setKey(key);
					objInfo.setValue(value);
					listData.add(objInfo);

					// System.out.println(key + " : " + value);
				}

			}

			String[] arrKey = new String[listData.size()];
			String[] arrValue = new String[listData.size()];

			for (int i = 0; i < listData.size(); i++) {
				arrKey[i] = listData.get(i).getKey();
				arrValue[i] = listData.get(i).getValue();
			}

			PNGCompany pngCompany = new PNGCompany();
			List<PgAddressPre> pgAddressPre = new ArrayList<PgAddressPre>();
			List<PgCompanyShareholderPre> pgCompanyShareholderPre = new ArrayList<PgCompanyShareholderPre>();
			;
			PgDetailPre pgDetailPre = new PgDetailPre();
			List<PgPersonPre> pgPersonPre = new ArrayList<PgPersonPre>();
			;
			List<PgSharePre> pgSharePre = new ArrayList<PgSharePre>();
			;

			boolean check = false;

			for (int i = 0; i < arrKey.length; i++) {
				String key = arrKey[i];
				if ("Company Name".equals(key)
						&& "Company Status".equals(arrKey[i + 2])) {

					pgDetailPre.setRegistName(id);
					pgDetailPre.setCompanyName(arrValue[i]);
					pgDetailPre.setEffDate(arrValue[i + 1]);
					pgDetailPre.setCompanyStatus(arrValue[i + 2]);
					pgDetailPre.setIncDate(arrValue[i + 3]);
					pgDetailPre.setOwnConstitution(arrValue[i + 4]);
					pgDetailPre.setMainBusSector(arrValue[i + 5]);
					pgDetailPre.setAnnualFilingMonth(arrValue[i + 6]);

					// /pgDetailPre.setDateOfAnnual(arrValue[i + 7]);
					// pgDetailPre.setAppName(arrValue[i + 8]);
					// pgDetailPre.setPostAddress(arrValue[i + 9]);

				}

				if ("Previous Name".equals(key)) {
					pgDetailPre.setPrevName(arrValue[i]);
					pgDetailPre.setStartDate(arrValue[i + 1]);
					pgDetailPre.setEndDate(arrValue[i + 2]);
				}

				if ("Registered Office Address".equals(key)
						&& "Start Date".equals(arrKey[i - 1])) {
					PgAddressPre item = new PgAddressPre();
					item.setRegistName(id);
					item.setStartDate(arrValue[i - 1]);
					item.setAddressType("REGIST");
					item.setAddress(arrValue[i]);
					pgAddressPre.add(item);
				}

				if ("Address for Service".equals(key)
						&& "Start Date".equals(arrKey[i - 1])) {
					PgAddressPre item = new PgAddressPre();
					item.setRegistName(id);
					item.setStartDate(arrValue[i - 1]);
					item.setAddressType("SERVICE");
					item.setAddress(arrValue[i]);
					pgAddressPre.add(item);
				}

				if ("Postal Address".equals(key)
						&& "Start Date".equals(arrKey[i - 1])) {
					PgAddressPre item = new PgAddressPre();
					item.setRegistName(id);
					item.setStartDate(arrValue[i - 1]);
					item.setAddressType("POSTAL");
					item.setAddress(arrValue[i]);
					pgAddressPre.add(item);
				}

				if ("This person has consented to act as a director for this company"
						.equals(key)) {
					PgPersonPre item = new PgPersonPre();
					item.setRegistName(id);
					if ("Name".equals(arrValue[i - 4])) {
						item.setName(arrValue[i - 4]);
					} else {
						item.setName(arrValue[i - 2]);
					}

					// item.setResidentialAddress(arrValue[i - 3]);
					if ("Postal Address".equals(arrValue[i - 2])) {
						item.setPostalAddress(arrValue[i - 2]);
					}

					item.setNationality(arrValue[i - 1]);
					item.setPersonType("DIR");
					item.setAppointmentDate(arrValue[i + 1]);
					item.setConsented(arrValue[i]);
					pgPersonPre.add(item);
				}

				if ("This person has consented to act as a shareholder for this company"
						.equals(key)) {

					PgPersonPre item = new PgPersonPre();
					item.setRegistName(id);
					item.setName(arrValue[i - 4]);
					item.setResidentialAddress(arrValue[i - 2]);
					item.setPostalAddress(arrValue[i - 1]);
					item.setNationality(arrValue[i - 1]);
					item.setPersonType("SHAREHOLDER");
					item.setAppointmentDate(arrValue[i + 1]);
					item.setConsented(arrValue[i]);
					pgPersonPre.add(item);
				}

				if ("This person has consented to act as a secretary for this company"
						.equals(key)) {
					PgPersonPre item = new PgPersonPre();
					item.setRegistName(id);
					item.setName(arrValue[i - 4]);
					item.setResidentialAddress(arrValue[i - 3]);
					item.setPostalAddress(arrValue[i - 2]);
					item.setNationality(arrValue[i - 1]);
					item.setPersonType("SECRETARY");
					item.setAppointmentDate(arrValue[i + 1]);
					item.setConsented(arrValue[i]);
					pgPersonPre.add(item);
				}

				if ("This company has consented to act as a shareholder for this company"
						.equals(key)) {

					PgCompanyShareholderPre item = new PgCompanyShareholderPre();
					item.setRegistName(id);
					item.setCompanyNumber(arrValue[i - 4]);
					item.setFullLegalName(arrValue[i - 3]);
					item.setOfficeAddress(arrValue[i - 2]);
					item.setPlaceOfIncorporation(arrValue[i - 1]);
					item.setConsented(arrValue[i]);
					item.setAppointmentDate(arrValue[i + 1]);

					pgCompanyShareholderPre.add(item);
				}

				if ("Total Shares".equals(key)) {
					pgDetailPre.setTotalShares(arrValue[i]);
					pgDetailPre.setExtensiveShareholding(arrValue[i + 1]);
					pgDetailPre.setMoreThanOne(arrValue[i + 2]);

				}
				if ("Number of shares".equals(key)
						&& "Company Number".equals(arrKey[i + 1])) {
					PgSharePre item = new PgSharePre();
					item.setRegistName(id);
					item.setNumberOfShares(arrValue[i]);
					item.setName(arrValue[i + 2]);
					item.setCompanyNumber(arrValue[i + 1]);
					pgSharePre.add(item);

				}
				if ("Number of shares".equals(key)
						&& ("Name".equals(arrKey[i + 1]) 
								|| "Full Legal Name"
								.equals(arrKey[i + 1])
								|| "Company Name or Number"
								.equals(arrKey[i + 1])								
								)
								) {

					PgSharePre item = new PgSharePre();
					item.setRegistName(id);

					item.setNumberOfShares(arrValue[i]);
					item.setName(arrValue[i + 1]);
					if ("Full Legal Name".equals(arrKey[i + 1])) {
						item.setCompanyNumber("N/A");
					}
					pgSharePre.add(item);

				}

			}
			if (pgAddressPre.isEmpty() == false
					&& pgPersonPre.isEmpty() == false
					&& checkDirector(pgPersonPre) == true

			) {
				pngCompany.setPgDetailPre(pgDetailPre);
				pngCompany.setPgAddressPre(pgAddressPre);
				pngCompany.setPgCompanyShareholderPre(pgCompanyShareholderPre);
				pngCompany.setPgPersonPre(pgPersonPre);
				pngCompany.setPgSharePre(pgSharePre);
				 new PNGDAO().insertCompany(pngCompany);
				 A03Dao.UpdateExtractStatus(id, 1);
			}

			// Elements listValue = document.select(".appAttrValue");
			int a = 0;
			document = null;

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	private static boolean checkDirector(List<PgPersonPre> pgPersonPre) {
		boolean result = false;
		for (PgPersonPre item : pgPersonPre) {
			if ("DIR".equals(item.getPersonType())) {
				result = true;
				break;
			}
		}
		return result;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// doSomething("1-103002");
		doExtract("1-104471");
		// doExtract("1-27053");
		// doExtract("1-247");
		// doExtract("3-8554");
	}

	public static void crawlVersion(CommandParameters commandParams) {
		try {

			// A03Dao.UpdateStatus("1-103002",2);
			mkDir();

			List<A03Model> listData = A03Dao.getListCompany(
					commandParams.getFrom(), commandParams.getTo());
			getTaskQueue(listData);
			int thread = commandParams.getThread();
			for (int i = 0; i < thread; i++) {
				getDataByCompanyVersion();
			}
		} catch (SQLException e) {
			logger.error(e);
		}

	}

	public static void crawl(CommandParameters commandParams) {
		try {

			// A03Dao.UpdateStatus("1-103002",2);
			mkDir();

			List<A03Model> listData = A03Dao.getListCompany(
					commandParams.getFrom(), commandParams.getTo());
			getTaskQueue(listData);
			int thread = commandParams.getThread();
			for (int i = 0; i < thread; i++) {
				getDataByCompany();
			}
		} catch (SQLException e) {
			logger.error(e);
		}

	}

	public static void crawl3(CommandParameters commandParams) {
		try {

			// A03Dao.UpdateStatus("1-103002",2);
			mkDir();

			List<A03Model> listData = A03Dao.getListCompanySecond(
					commandParams.getFrom(), commandParams.getTo());
			getTaskQueue(listData);
			int thread = commandParams.getThread();
			for (int i = 0; i < thread; i++) {
				getDataByCompanySecond();
			}
		} catch (SQLException e) {
			logger.error(e);
		}

	}

}
