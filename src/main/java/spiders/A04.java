/**
 * 
 */
package spiders;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import model.A04Model;
import model.parameters.CommandParameters;

import org.apache.commons.lang.time.DateUtils;
import org.jsoup.Connection;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import common.RequestConstants;
import exporters.csv.CommodityExporter;

/**
 * @author thi
 *
 */
public class A04 extends SpiderBase {
	CommodityExporter exporter;
	int type;
	public static String datePattern = "MM/dd/yyyy";

	public A04(int type, CommandParameters commandParams) {
		exporter = new CommodityExporter();
		this.commandParams = commandParams;
		this.type = type;
	}

	@Override
	public void createContentDB() {
		// TODO Auto-generated method stub

	}

	@Override
	public void getData() {
		switch (type) {
		case RequestConstants.COMMODITY_DAILY:
			daily();
			break;
		case RequestConstants.COMMODITY_HISTORY:
			history();
			break;
		default:
			break;
		}

	}

	public void history() {
		List<A04Model> listData = new ArrayList<A04Model>();
		String id = "0";
		try {

			Date endDate = utils.DateUtils.getDateFromString(commandParams
					.getEndDate());

			Date startDate = utils.DateUtils.getDateFromString(commandParams
					.getStartDate());
			String dateEnd = utils.DateUtils.convertToOtherFormat(
					commandParams.getEndDate(), datePattern);
			String dateStart = utils.DateUtils.convertToOtherFormat(
					commandParams.getStartDate(), datePattern);

			String seedURL = "http://www.investing.com";
			// Document doc =
			// getDocument("http://www.ipa.gov.pg/pngmaster/service/create.html?targetAppCode=pngmaster&targetRegisterAppCode=pngcompanies&service=registerItemSearch&target=pngmaster");
			Document doc = getDocument("http://www.investing.com/commodities/real-time-futures");

			Element eRoot = doc.select("#cross_rates_container").first();
			Elements rows = eRoot.select("table>tbody>tr");
			for (Element row : rows) {
				currentURL = row.select("td>a").first().attr("href").toString();

				currentURL = seedURL + currentURL;
				doc = getDocument(currentURL);
				// console(doc.toString());
				currentURL = doc.select("#pairSublinksLevel2>li>a")
						.attr("href").toString();

				currentURL = seedURL + currentURL;
				String referURL = currentURL;
				doc = getDocument(currentURL);

				A04Model model = new A04Model();
				Element eData = doc.select("#leftColumn").first();
				model.setUnit(eData
						.select(".overViewBox.instrument>.right>div:eq(2)>span:eq(1)")
						.first().text());

				model.setTitle(eData.select("h1").first().text());
				String ccyCode = eData.select("span[class=bold]").first()
						.text();
				model.setCcyCode(ccyCode);
				id = doc.select("div").attr("pair_ID");
				model.setId(Integer.parseInt(id));

				currentURL = String.format(startURL, id, dateStart, dateEnd);
				// request body
				Map<String, String> datamap = new HashMap<String, String>();
				// datamap.put("_CBHTMLFRAGID_","1435834449047");
				datamap.put("action", "historical_data");
				datamap.put("curr_id", id);
				datamap.put("st_date", dateStart);
				datamap.put("end_date", dateEnd);
				datamap.put("interval_sec", "Daily");

				doc = getDocument(currentURL, referURL, datamap);

				Elements dataRows = doc.select("#results_box>table>tbody>tr");
				for (Element element : dataRows) {

					A04Model rowModel = model;
					model.setTime(element.select("td:eq(0)").first().text());
					model.setPrice(element.select("td:eq(1)").first().text());
					listData.add(rowModel);
					console(rowModel.toString());
				}

				// console(id);
			}
			/*
			 * for (A04Model item:listData) { console(item.toString()); }
			 */
			exporter.exportToCSV(listData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			console(id);
			e.printStackTrace();
		}

		System.out.println("va0 A03");
	}

	public void daily() {
		List<A04Model> listData = new ArrayList<A04Model>();

		try {
			startURL = "http://www.investing.com";
			// Document doc =
			// getDocument("http://www.ipa.gov.pg/pngmaster/service/create.html?targetAppCode=pngmaster&targetRegisterAppCode=pngcompanies&service=registerItemSearch&target=pngmaster");
			Document doc = getDocument("http://www.investing.com/commodities/real-time-futures");

			Element eRoot = doc.select("#cross_rates_container").first();
			Elements rows = eRoot.select("table>tbody>tr");
			for (Element row : rows) {
				String nextURL = row.select("td>a").first().attr("href")
						.toString();
				nextURL = startURL + nextURL;
				doc = getDocument(nextURL);
				// console(doc.toString());
				nextURL = doc.select("#pairSublinksLevel2>li>a").attr("href")
						.toString();
				nextURL = startURL + nextURL;
				doc = getDocument(nextURL);

				A04Model model = new A04Model();
				Element eData = doc.select("#leftColumn").first();
				model.setUnit(eData
						.select(".overViewBox.instrument>.right>div:eq(2)>span:eq(1)")
						.first().text());

				model.setTitle(eData.select("h1").first().text());
				String ccyCode = eData.select("span[class=bold]").first()
						.text();
				model.setCcyCode(ccyCode);
				String id = doc.select("div").attr("pair_ID");
				model.setId(Integer.parseInt(id));
				eData = doc.select("#curr_table>tbody").first();

				Date curDate = new Date();
				String datePattern = "MMM dd,yyyy";
				SimpleDateFormat formater = new SimpleDateFormat(datePattern);
				Date lastDate = formater.parse(eData.select("tr").get(0)
						.select("td").get(0).text());

				if (DateUtils.isSameDay(curDate, lastDate) == true) {
					model.setTime(eData.select("tr").get(1).select("td").get(0)
							.text());
					model.setPrice(eData.select("tr").get(1).select("td")
							.get(1).text());
				} else {
					model.setTime(eData.select("tr").get(0).select("td").get(0)
							.text());
					model.setPrice(eData.select("tr").get(0).select("td")
							.get(1).text());
				}

				listData.add(model);
				console(model.toString());

			}
			exporter.exportToCSV(listData);
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

		System.out.println("va0 A03");
	}

	public static void main(String[] args) {
		// new A04().getData();
	}

	@Override
	public void exportData() {
		// TODO Auto-generated method stub

	}

}
