/**
 * 
 */
package spiders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.apache.commons.lang.StringUtils;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.google.gson.Gson;

import common.RequestConstants;
import core.selenium.factory.WebDriverController;
import core.selenium.factory.WebDriverFactory;
import dao.B02Dao;
import exporters.csv.CommodityExporter;
import model.B02.CustomizationChoices;
import model.B02.Image;
import model.B02.JsonModel;
import model.B02.Option;
import model.B02.Product;
import model.parameters.CommandParameters;
import net.sourceforge.htmlunit.corejs.javascript.ast.ArrayLiteral;
import oracle.net.aso.e;

/**
 * @author thi
 *
 */
public class B02 extends SpiderBase {
	B02Dao dao = new B02Dao();
	int type;

	public B02() {

	}

	public B02(int type, CommandParameters commandParams) {
		this.commandParams = commandParams;
		this.type = type;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see spiders.SpiderBase#createContentDB()
	 */
	@Override
	public void createContentDB() {
		// TODO Auto-generated method stub

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see spiders.SpiderBase#getData()
	 */

	public void getOldData() {
		console("Start getOldData()");
		List<Product> products = dao.getListOldProduct();
		List<Product> listEmptyProduct = new ArrayList<Product>();
		for (Product item : products) {
			if (dao.chkProductIDExisted(Integer.parseInt(item.getId())) == true
					|| dao.chkProductURLExisted(item.getUrl()) == true)
				continue;
			Product product = getProductByURL(item.getUrl(), 1);
			if (product != null) {
				String json;
				product.setTitle(item.getTitle());
				json = new Gson().toJson(product);
				dao.insert(Integer.parseInt(item.getId()), json, 1, product.getUrl());
				console(json);
			} else {
				listEmptyProduct.add(item);
			}
		}
		console("list empty == " + new Gson().toJson(listEmptyProduct));
	}

	public void getAllData() {
		console("Start getAllData()");
		List<JsonModel> listJsonModel = dao.getListJson();
		int count=0;
		for (JsonModel item : listJsonModel) {
			count++;
			console(count);
			
			int productID = item.getId();
			String oldJson = item.getValue();
			String url = StringUtils.substringBetween(oldJson, ",\"url\":\"", "\",\"images\":");
			document = getDocument(url);
			String quantity = null;
			Elements eQuantities = document.select("#product_addtocart_form > div.product-shop > div.tmg-quantity>div");
			List<String> quantities = new ArrayList<String>();
			for (Element element : eQuantities) {
				quantities.add(element.select("label").text());
			}

			if (eQuantities.size() > 0) {
				quantity = new Gson().toJson(quantities);
				console(quantity);
				 dao.updateQuantity(productID, quantity);
			}
			
			/*
			 * if (productID == 4284) { getProductByURL(url,1); }
			 */
		}
	}

	public Product getProductByURL(String productLink, int type) {

		Product product = new Product();
		dao = new B02Dao();
		int productID = dao.getProductIDByURL(productLink);
		if (productID == 0) {
			product.setId("");
		} else {
			product.setId(new Integer(productID).toString());
		}
		type = productID == 0 ? 0 : 1;
		// product.setImageURL(element.select("a>img").attr("src").toString());
		// document = getDocument(productLink);
		driver = getWebDriver(BrowserType.CHROME);
		driver.get(productLink);
		WebElement eErrorHeader = null;
		try {
			driver.manage().timeouts().implicitlyWait(0, TimeUnit.SECONDS);
			eErrorHeader = driver.findElement(By.cssSelector(
					"body > div.wrapper > div > div.main-container.col1-layout > div > div > div > div.page-head-alt > h3"));
		} catch (Exception e2) {
			e2.printStackTrace();
			eErrorHeader = null;
		}
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);

		if (eErrorHeader != null) {
			String checkHeader = eErrorHeader.getText();
			console(checkHeader);
			if ("WE ARE SORRY, BUT THE PAGE YOU ARE LOOKING FOR CANNOT BE FOUND.".equals(checkHeader.trim()))
				return null;
		}
		// product.setTitle(driver.findElement(By.cssSelector("#product-name-scroll
		// > span")).getText());
		// console("title== " +
		// driver.findElement(By.cssSelector("#product-name-scroll >
		// span")).getText());
		console(productLink);
		product.setUrl(productLink);
		WebDriverController controller = new WebDriverController(driver);

		List<Image> images = new ArrayList<Image>();
		List<WebElement> eImages = driver.findElements(By.cssSelector("#color-swatch>span"));
		for (WebElement element2 : eImages) {

			Image image = new Image();
			image.setAttributeValue(element2.getAttribute("title").toString());
			image.setAttributeName("color");
			console(image.getAttributeValue());
			try {
				controller.clickElement(element2);
			} catch (WebDriverException e1) {
				// TODO Auto-generated catch block
				try {
					Thread.sleep(5000);
				} catch (InterruptedException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				controller.clickElement(element2);
				e1.printStackTrace();
			}
			// controller.waitLoaderCompleted(By.cssSelector("body >
			// div.zoomContainer > div.zoomWindowContainer > div"));

			try {
				String imgSrc = driver
						.findElement(By.cssSelector("body > div.zoomContainer > div.zoomWindowContainer > div"))
						.getAttribute("style").toString();
				imgSrc = StringUtils.substringBetween(imgSrc, "url(\"", "\")");
				// console(image.getAttributeValue() + ":" +
				// imgSrc);
				image.setImageSource(imgSrc);
			} catch (Exception e) {
				// TODO Auto-generated catch block
				type = 2;
				e.printStackTrace();
			}
			images.add(image);

		}

		List<CustomizationChoices> customizationChoices = new ArrayList<CustomizationChoices>();

		CustomizationChoices customizationChoice = new CustomizationChoices();
		customizationChoice.setChoiceName(
				driver.findElement(By.cssSelector("#customize-option > div:nth-child(2) > p")).getText());
		List<String> options = new ArrayList<String>();
		WebElement ePrint = driver.findElement(By.cssSelector("#imprint-location"));
		List<WebElement> eOptions = driver.findElements(By.cssSelector("#imprint-location > option"));
		customizationChoice.setPlaceholder(eOptions.get(0).getText());
		eOptions.remove(0);
		int count = 0;
		for (WebElement webElement : eOptions) {
			options.add(webElement.getText());
		}
		customizationChoice.setOptions(options);
		customizationChoices.add(customizationChoice);

		WebElement eColor = driver.findElement(By.cssSelector("#imprint-location-color-count"));
		String style = eColor.getAttribute("style");
		console("style ==" + style);

		// style = StringUtils.substringBetween(style, "", "");
		if (StringUtils.isBlank(style)) {

			Select selectPrint = new Select(ePrint);
			if (selectPrint.getOptions().size() > 1) {
				selectPrint.selectByIndex(1);
				Select selectColor = new Select(eColor);
				selectColor.selectByIndex(1);

				customizationChoice = new CustomizationChoices();
				eOptions = eColor.findElements(By.tagName("option"));
				customizationChoice.setPlaceholder(eOptions.get(0).getText());
				eOptions.remove(0);
				count = 0;
				options = new ArrayList<String>();
				for (WebElement webElement : eOptions) {
					options.add(webElement.getText());
				}
				customizationChoice.setOptions(options);
				customizationChoices.add(customizationChoice);
				// custom 3

				customizationChoice = new CustomizationChoices();
				eColor = driver.findElements(By.cssSelector("#imprint-color-blk>ul")).get(0);
				eOptions = eColor.findElements(By.cssSelector("li"));
				console(eOptions.get(0).getText());
				customizationChoice.setPlaceholder(eOptions.get(0).getText());
				eOptions.remove(0);
				count = 0;
				options = new ArrayList<String>();
				List<Option> listNewOption = new ArrayList<Option>();
				for (WebElement webElement : eOptions) {
					count++;
					if (count == eOptions.size())
						break;
					Option option = new Option();
					option.setName(StringUtils.substringAfter(webElement.getAttribute("innerHTML"), "</span>"));
					option.setValue(StringUtils
							.substringBetween(webElement.getAttribute("innerHTML"), "background:", ";").trim());
					listNewOption.add(option);
					options.add(StringUtils.substringAfter(webElement.getAttribute("innerHTML"), "</span>"));
				}
				// listNewOption.remove(listNewOption.size()-1);
				console(new Gson().toJson(listNewOption));
				customizationChoice.setOptions(options);
				customizationChoices.add(customizationChoice);
			}
			console("OK");

		}

		product.setImages(images);
		product.setChoices(customizationChoices);
		return product;
	}

	@Override
	public void getData() {
		switch (type) {
		case RequestConstants.B02_NEWDATA:
			getNewData();
			break;
		case RequestConstants.B02_OLDDATA:
			getOldData();
			break;
		default:
			break;
		}
	}

	public void getNewData() {
		// TODO Auto-generated method stub
		console("Start getNewData()");
		startURL = "http://www.swagstandard.com/";
		document = getDocument(startURL);
		Elements eCategories = document.select("#nav > ol > li.level0.nav-7.parent > ul > li>a");
		int countCat = 0;
		List<Product> products = new ArrayList<Product>();
		List<Product> newProducts = new ArrayList<Product>();
		int nbrProduct = 1;
		for (Element eCat : eCategories) {
			countCat++;

			/*
			 * if (countCat < 6) continue;
			 */
			if (countCat > 9)
				break;
			console(eCat.attr("href"));
			String catLink = eCat.attr("href");
			String catName = eCat.text();
			document = getDocument(catLink);

			String countProduct = document
					.select("body > div.wrapper > div > div.main-container.col2-left-layout > div > div.col-main > div.category-products > div.toolbar-top > div > div.pager > div.count-container > p")
					.text();
			countProduct = StringUtils.substringAfter(countProduct, "of").trim();
			console(countProduct);
			int pageCount = Integer.parseInt(countProduct) / 24;
			pageCount = pageCount % 24 == 0 ? pageCount : pageCount + 1;
			int type = 1;
			for (int i = 0; i < pageCount; i++) {

				console("page ==" + (i + 1));
				/*
				 * if (countCat == 2 && i < 25) continue;
				 */
				document = getDocument(catLink + "?p=" + (i + 1));

				Elements eProducts = document.select(
						"body > div.wrapper > div > div.main-container.col2-left-layout > div > div.col-main > div.category-products > ul > li");
				int runCount = 0;
				for (Element element : eProducts) {
					/*
					 * if (runCount > 99) break;
					 */
					runCount++;

					Product product = new Product();
					console(nbrProduct);
					nbrProduct++;
					String productLink = element.select("#longText").attr("href").toString();
					B02Dao dao = new B02Dao();
					int productID = dao.getProductIDByURL(productLink);
					if (dao.chkProductIDExisted(productID) == true || dao.chkProductURLExisted(productLink) == true)
						continue;
					if (productID == 0) {
						product.setId("");
					} else {
						product.setId(new Integer(productID).toString());
					}
					product.setCategory(catName);
					type = productID == 0 ? 0 : 1;
					product.setImageURL(element.select("a>img").attr("src").toString());
					// document = getDocument(productLink);
					driver = getWebDriver(BrowserType.CHROME);
					driver.get(productLink);
					product.setTitle(driver.findElement(By.cssSelector("#product-name-scroll > span")).getText());
					// console("title== " +
					// driver.findElement(By.cssSelector("#product-name-scroll >
					// span")).getText());
					console(productLink);
					product.setUrl(productLink);
					WebDriverController controller = new WebDriverController(driver);

					List<Image> images = new ArrayList<Image>();
					List<WebElement> eImages = driver.findElements(By.cssSelector("#color-swatch>span"));
					for (WebElement element2 : eImages) {

						Image image = new Image();
						image.setAttributeValue(element2.getAttribute("title").toString());
						image.setAttributeName("color");
						console(image.getAttributeValue());
						try {
							controller.clickElement(element2);
						} catch (WebDriverException e1) {
							// TODO Auto-generated catch block
							try {
								Thread.sleep(5000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							controller.clickElement(element2);
							e1.printStackTrace();
						}
						// controller.waitLoaderCompleted(By.cssSelector("body >
						// div.zoomContainer > div.zoomWindowContainer > div"));

						try {
							String imgSrc = driver
									.findElement(
											By.cssSelector("body > div.zoomContainer > div.zoomWindowContainer > div"))
									.getAttribute("style").toString();
							imgSrc = StringUtils.substringBetween(imgSrc, "url(\"", "\")");
							// console(image.getAttributeValue() + ":" +
							// imgSrc);
							image.setImageSource(imgSrc);
						} catch (Exception e) {
							// TODO Auto-generated catch block
							type = 2;
							e.printStackTrace();
						}
						images.add(image);

					}

					List<CustomizationChoices> customizationChoices = new ArrayList<CustomizationChoices>();

					CustomizationChoices customizationChoice = new CustomizationChoices();
					customizationChoice.setChoiceName(
							driver.findElement(By.cssSelector("#customize-option > div:nth-child(2) > p")).getText());
					List<String> options = new ArrayList<String>();
					WebElement ePrint = driver.findElement(By.cssSelector("#imprint-location"));
					List<WebElement> eOptions = driver.findElements(By.cssSelector("#imprint-location > option"));
					customizationChoice.setPlaceholder(eOptions.get(0).getText());
					eOptions.remove(0);
					int count = 0;
					for (WebElement webElement : eOptions) {
						options.add(webElement.getText());
					}
					customizationChoice.setOptions(options);
					customizationChoices.add(customizationChoice);

					WebElement eColor = driver.findElement(By.cssSelector("#imprint-location-color-count"));
					String style = eColor.getAttribute("style");
					console("style ==" + style);

					// style = StringUtils.substringBetween(style, "", "");
					if (StringUtils.isBlank(style)) {

						Select selectPrint = new Select(ePrint);
						if (selectPrint.getOptions().size() > 1) {
							selectPrint.selectByIndex(1);
							Select selectColor = new Select(eColor);
							selectColor.selectByIndex(1);

							customizationChoice = new CustomizationChoices();
							eOptions = eColor.findElements(By.tagName("option"));
							customizationChoice.setPlaceholder(eOptions.get(0).getText());
							eOptions.remove(0);
							count = 0;
							options = new ArrayList<String>();
							for (WebElement webElement : eOptions) {
								options.add(webElement.getText());
							}
							customizationChoice.setOptions(options);
							customizationChoices.add(customizationChoice);
							// custom 3

							customizationChoice = new CustomizationChoices();
							eColor = driver.findElements(By.cssSelector("#imprint-color-blk>ul")).get(0);
							eOptions = eColor.findElements(By.cssSelector("li"));
							console(eOptions.get(0).getText());
							customizationChoice.setPlaceholder(eOptions.get(0).getText());
							eOptions.remove(0);
							count = 0;
							options = new ArrayList<String>();
							for (WebElement webElement : eOptions) {
								options.add(
										StringUtils.substringAfter(webElement.getAttribute("innerHTML"), "</span>"));
							}
							customizationChoice.setOptions(options);
							customizationChoices.add(customizationChoice);
						}
						console("OK");

					}

					product.setImages(images);
					product.setChoices(customizationChoices);
					String json;

					json = new Gson().toJson(product);
					dao.insert(productID, json, type, productLink);

					console(json);
				}
			}
		}
		// console(new Gson().toJson(products));
		// console(new Gson().toJson(newProducts));

	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see spiders.SpiderBase#exportData()
	 */
	@Override
	public void exportData() {
		// TODO Auto-generated method stub

	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		B02 crawler = new B02();
		crawler.getAllData();
		// 1.1 get from current site
		// crawler.getData();
		// 1.2 get old url from database
		// crawler.getOldProduct();
	}

}
