/**
 * 
 */
package spiders;

import java.io.IOException;
import java.sql.SQLException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;

import javax.swing.text.html.FormSubmitEvent.MethodType;

import model.A04Model;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.Connection.Method;
import org.jsoup.Connection.Response;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import dao.A02Dao;
import dao.A04Dao;
import dao.BaseDao;

/**
 * @author thi
 *
 */
public class A044 extends SpiderBase {
	A04Dao daoContent;
	static Logger logger = LogManager.getLogger(A02.class.getName());

	public A044() {
		daoContent = new A04Dao();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		new A044().getData();
	}

	@Override
	public void createContentDB() {
		// TODO Auto-generated method stub
		daoContent.createTables();
	}

	@Override
	public void getData() {
		// Document doc =
		// getDocument("http://www.ipa.gov.pg/pngmaster/service/create.html?targetAppCode=pngmaster&targetRegisterAppCode=pngcompanies&service=registerItemSearch&target=pngmaster");
		// getDataByTab("Energy","1");
		// getDataByTab("Metals","2");
		getDataByTab("Agricultural+%26+Lumber", "3");
	}



	private void getDataByTab(String tabName, String type) {

		Document doc = null;
		String url = "http://markets.ft.com/research/Markets/Commodities";
		Connection con = null;
		con = Jsoup.connect(url);
		con.header("Host", "markets.ft.com");
		con.userAgent("Mozilla/5.0 (Windows NT 5.1; rv:39.0) Gecko/20100101 Firefox/39.0");
		con.header("Accept", "application/json, text/javascript, */*; q=0.01");
		con.header("Accept-Language", "en-US,en;q=0.5");
		con.header("Accept-Encoding", "gzip, deflate");

		con.header("Content-Type", "application/json; charset=utf-8");
		// con.header("Content-Type",
		// "application/json, text/javascript, */*; q=0.01");
		con.header("X-Requested-With", "XMLHttpRequest");
		// con.header("Referer",
		// "http://markets.ft.com/research/Markets/Commodities");
		con.header("Connection", "keep-alive");
		con.header("Pragma", "no-cache");
		con.header("Cache-Control", "no-cache");

		con.method(Method.POST);
		// con.ignoreContentType(true);

		// request body
		Map<String, String> datamap = new HashMap<String, String>();
		// datamap.put("_CBHTMLFRAGID_","1435834449047");
		datamap.put("tabSelection", tabName);
		datamap.put("dataStore", "");
		// datamap.put("_VIKEY_", "810d7f2cx59edx486exb31dx37666facd993");
		// datamap.put("_CBNAME_", "pageSizeChange");
		datamap.put("ResetPaging", "true");
		datamap.put(
				"ODA_Parent",
				"%7B%22category%22%3A%22Commodities%22%2C%22name%22%3A%22Overview%22%2C%22label%22%3A%22Overview%22%7D");
		// con.timeout(1800);

		// con.data(datamap);

		try {
			// Connection.Response res = con.execute();

			// String contentType=res.contentType();
			doc = con.post();
			console(doc.toString());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		Elements eRows = doc.select(".sortableTable>tbody>tr");
		A04Model model = new A04Model();
		for (Element row : eRows) {
			String tile = row.select("td.firstCol > div > a").toString();
			model.setTitle(row.select("td.firstCol > div > a").text());
			model.setTime(row.select("td.firstCol > div > span").text());

			Element eTd2 = row.select("td:nth-child(2)").first();
			// row.select("td[2]/text()");

			model.setCcyCode(eTd2.select("span").text());
			eTd2.select("br").remove();
			eTd2.select("span").remove();
			model.setPrice(eTd2.text());

			model.setChange(row.select(
					"td:nth-child(3) > span.neg.color.stackedPriceChange")
					.text());
			model.setNextUrl(row.select("td.firstCol > div > a").attr("href"));
			model.setType(type);
			daoContent.createData(model);
			int x = 0;
		}
		// console(doc.toString());
	}

	@Override
	public void exportData() {
		// TODO Auto-generated method stub

	}

}
