package spiders;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.net.MalformedURLException;
import java.net.URL;
import java.text.Format;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;
import java.util.Scanner;

import model.CCY;
import model.CurrencyRate;
import model.crawler.RequestFail;
import model.parameters.CommandParameters;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;

import utils.DateUtils;
import utils.QueryStringUtils;

import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonParser;

import common.RequestConstants;
import common.SettingCommon;
import dao.A01Dao;
import dao.A02Dao;
import dao.BaseDao;
import dao.CommonDAO;
import exporters.converter.Converter;
import exporters.oracle.dao.BaseDAOExp;
import exporters.oracle.dao.OandaCurrencyRatesDAOExp;

/**
 * @author thi
 *
 */
public class A01 extends SpiderBase {
	public static String datePattern = "yyyy-M-d";
	static Logger logger = LogManager.getLogger(A01.class.getName());
	A01Dao daoContent;
	CommonDAO daoCommon;
	OandaCurrencyRatesDAOExp exporter;

	int type;
	String ccy;

	public A01() {
		daoContent = new A01Dao();
		exporter = new OandaCurrencyRatesDAOExp();
	}

	public A01(int type, CommandParameters commandParams) {
		super();
		daoContent = new A01Dao();
		daoCommon = new CommonDAO();
		this.type = type;
		this.commandParams = commandParams;
		this.ccy = commandParams.getCcy();
		exporter = new OandaCurrencyRatesDAOExp();
	}

	public void getData() {

		switch (type) {
		case RequestConstants.RUN_DAILY_CYY:
			runDaily();
			break;
		case RequestConstants.RUN_NEW_CCY:
			runNewCCY();
			break;
		case RequestConstants.RUN_OLD_CYY:
			runOldCCY();
			break;
		case RequestConstants.RUN_REQ_CCY_FAIL:
			runReqFailCCY();
			break;
		case RequestConstants.RUN_CCY:
			runCCY();
			break;
		default:
			break;
		}

	}

	public void runCCY() {
		try {

			Date endDate = DateUtils.getDateFromString(
					commandParams.getEndDate(), datePattern);

			Date startDate = DateUtils.getDateFromString(
					commandParams.getStartDate(), datePattern);
			List<CCY> listCCY = new ArrayList<CCY>();
			CCY ccyObj = new CCY();
			ccyObj.setValue(this.ccy);
			listCCY.add(ccyObj);
			insertBatchData(listCCY, startDate, endDate);
		} catch (Exception e) {
			logger.error(e.getMessage());

		}
	}

	public void runReqFailCCY() {
		List<RequestFail> listReqFail = getListReqFail(daoContent);
		List<CurrencyRate> listCCYRate = null;
		for (RequestFail reqFail : listReqFail) {
			listCCYRate = getCurrcyRate(reqFail.getUrlReq());
			String startDate = null;
			String endDate = null;
			try {
				URL url = new URL(reqFail.getUrlReq());
				String query = url.getQuery();
				Map<String, String> params = QueryStringUtils
						.getQueryMap(query);
				startDate = params.get("start_date");
				endDate = params.get("end_date");
				if (startDate != null && startDate.equals(endDate) == true
						&& listCCYRate.size() > 1) {
					listCCYRate.remove(1);
				}
			} catch (MalformedURLException e) {
				logger.error(e);
			}

			if (listCCYRate != null) {
				insertBatchCurrencyRate(listCCYRate);
				deleteReqFail(daoContent, reqFail);

			}

		}
	}

	public void runDaily_OLD() {
		try {

			Date currentDate = new Date();
			List<String> listURL = getListURLVisit(currentDate, currentDate);
			for (String url : listURL) {
				currentURL = url;
				List<CurrencyRate> listCCYRate = getCurrcyRate(currentURL);
				for (CurrencyRate ccyRate : listCCYRate) {
					daoContent.createCurrencyRate(ccyRate);
					break;
				}
			}
		} catch (Exception e) {
			logger.error(e.getMessage());

		}
	}

	private void runNewCCY() {
		Date endDate = DateUtils.getDateFromString(commandParams.getEndDate(),
				datePattern);
		Date startDate = new Date(631152000000L);
		List<CCY> listCCY = daoCommon.getListNewCCY();
		insertBatchData(listCCY, startDate, endDate);

	}

	private void runOldCCY() {
		Date endDate = DateUtils.getDateFromString(commandParams.getEndDate(),
				datePattern);

		Date startDate = DateUtils.getDateFromString(
				commandParams.getStartDate(), datePattern);

		if (endDate == null) {
			endDate = new Date();
		}
		System.out.println(endDate.getTime() == startDate.getTime());
		List<CCY> listCCY = daoCommon.getListCCY();
		insertBatchData(listCCY, startDate, endDate);
	}

	private void runDaily() {
		Date endDate = new Date();
		Date startDate = new Date();
		List<CCY> listCCY = daoCommon.getListCCY();
		insertBatchData(listCCY, startDate, endDate);

	}

	private void insertBatchData(List<CCY> listCCY, Date startDate, Date endDate) {
		if (listCCY==null || listCCY.isEmpty()==true) return;
		List<CurrencyRate> listCCYRate = null;
		String dateStart = DateUtils.getStringByDate(startDate, datePattern);
		String dateEnd = DateUtils.getStringByDate(endDate, datePattern);
		log.info("startDate==" + dateStart);
		log.info("endDate==" + dateEnd);
		for (CCY ccy : listCCY) {
			currentURL = String.format(startURL, "USD", dateEnd, dateStart,
					ccy.getValue());
			listCCYRate = getCurrcyRate(currentURL);
			if (listCCYRate == null)
				continue;
			if (dateStart.equals(dateEnd) == true && listCCYRate.size() > 1) {
				listCCYRate.remove(1);
			}
			// log.info("CCYRate=="+dateEnd);
			insertBatchCurrencyRate(listCCYRate);
			// daoCommon.updateCCYStatus(ccy);

		}

	}

	private void insertBatchCurrencyRate(List<CurrencyRate> listCurrencyRate) {
		/*
		 * for (CurrencyRate ccyRate : listCurrencyRate) {
		 * daoContent.createCurrencyRate(ccyRate); }
		 */
		if (listCurrencyRate==null || listCurrencyRate.isEmpty()==true) return;
		daoContent.createCurrencyRateList(listCurrencyRate);
	}

	private List<String> getListURLVisit(Date startDate, Date endDate) {
		List<String> listURL = new ArrayList<String>();
		String startDateString = DateUtils
				.getStringByDate(endDate, datePattern);
		String endDateString = DateUtils.getStringByDate(endDate, datePattern);
		List<CCY> listCCY = getListCCY();
		for (CCY item : listCCY) {
			currentURL = String.format(startURL, "USD", endDateString,
					startDateString, item.getValue());
			listURL.add(currentURL);

		}
		return listURL;
	}

	public void cleanDataOut() {
		File f = new File("data" + File.separator + "currencty_rate.txt");
		if (f.exists() == true) {
			f.delete();
		}
	}

	public List<CCY> getListCCY() {
		List<CCY> listCCY = daoCommon.getListCCY();
		return listCCY;
	}

	public List<CurrencyRate> getCurrcyRate(String currentURL) {
		List<CurrencyRate> listCurrencyRate = new ArrayList<CurrencyRate>();
		try {

			spiderInstance.setLastURLReq(currentURL);
			updateSpiderInstance(spiderInstance);
			Document doc = getDocument(currentURL);

			// insertReqFail();
			if (doc == null) {
				insertReqFail(daoContent);
				fail++;
				return null;
			} else {
				success++;
			}

			String result = doc.select("body").text();

			JsonParser parser = new JsonParser();
			JsonElement jsonElement = parser.parse(result).getAsJsonObject()
					.getAsJsonArray("widget").get(0);
			JsonArray arrayData = jsonElement.getAsJsonObject().getAsJsonArray(
					"data");
			String quoteCurrency = jsonElement.getAsJsonObject()
					.get("quoteCurrency").getAsString();
			String baseCurrency = jsonElement.getAsJsonObject()
					.get("baseCurrency").getAsString();

			for (JsonElement item : arrayData) {
				String data = item.toString();
				data = data.replace('[', ' ').replace(']', ' ').trim();
				String date = data.split(",")[0].trim();
				String rate = data.split(",")[1].trim();
				CurrencyRate currencyRate = new CurrencyRate();
				currencyRate.setBaseCurrency(baseCurrency);
				currencyRate.setQuoteCurrency(quoteCurrency);
				currencyRate.setDate(DateUtils.getDateInString(new Long(date)));
				currencyRate.setRate(new Double(rate));
				listCurrencyRate.add(currencyRate);
			}

		} catch (Exception e) {
			logger.error(e.getMessage());

		}
		return listCurrencyRate;
	}

	@Override
	public void createContentDB() {
		daoContent.createTables();

	}

	@Override
	public void exportData() {
		List<CurrencyRate> listCCYRate = daoContent.getListCCYRate();
		exporter.insertListCCYRate(Converter.convertListCCYRate(listCCYRate));

	}

}
