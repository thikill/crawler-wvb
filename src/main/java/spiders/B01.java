package spiders;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.openqa.selenium.By;

import com.google.gson.Gson;

import model.B01Model;

public class B01 extends SpiderBase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		B01 crawler = new B01();
		//crawler.getData();
		crawler.getProductsByShopName("MaisonMarrakech");
	}

	@Override
	public void createContentDB() {
		// TODO Auto-generated method stub

	}

	@Override
	public void getData() {
		// TODO Auto-generated method stub
		String keyWord = "moroccan";
		startURL = "http://www.ebay.com/sch/i.html?_from=R40&_sacat=0&_nkw=%s&_pgn=%s&_skc=%s&rt=nc";
		startURL = String.format(startURL, keyWord, 1, 50);
		document = getDocument(startURL);

		int totalItems = Integer
				.parseInt(document.select(".listingscnt").text().replaceAll(",", "").replace("listings", "").trim());
		int pageCount = totalItems / 50 + 1;
		List<B01Model> listData = new ArrayList<B01Model>();

		for (int i = 0; i < pageCount; i++) {
			if (i > 1)
				break;

			currentURL = String.format(startURL, keyWord, i + 1, 50);
			document = getDocument(currentURL);
			Elements eItems = document.select("#ListViewInner>li");
			for (Element element : eItems) {
				B01Model model = new B01Model();
				model.setTitle(element.select("h3").text());
				model.setImageLink(element.select("div > div > a > img").attr("src").toString());
				model.setPrice(element.select("ul.lvprices.left.space-zero > li.lvprice.prc > span").text());
				String producLink = element.select("h3 > a").attr("href").toString();
				model.setProductLink(producLink);
				document = getDocument(producLink);
				String desc = document.select("#vi-desc-maincntr > div.itemAttr").first().text();
				desc = desc.length() > 1000 ? desc.substring(1, 1000) : desc;
				model.setDescription(desc);
				listData.add(model);

			}

		}
		console(new Gson().toJson(listData));

		console(totalItems);

	}
	
	public void getProductsByShopName(String shopName) {
		startURL = "https://www.etsy.com/shop/%s?page=%s";
		startURL = String.format(startURL, shopName,1);
		document = getDocument(startURL);
		Element ePageLast = document.select("#pager-wrapper > div > ul>li").last();
		int pageCount = Integer.parseInt(ePageLast.select("a").text());
		console(pageCount);
		List<B01Model> listData = new ArrayList<B01Model>();
		int count = 1;
		for (int i = 0; i < pageCount; i++) {
			if (count > 100)
				break;
			currentURL = String.format(startURL,shopName, i + 1);
			document = getDocument(currentURL);
			Elements eItems = document.select("#listing-wrapper > ul>li");
			for (Element element : eItems) {
				B01Model model = new B01Model();
				model.setTitle(element.select("div.listing-detail > div.listing-title > a").text());
				model.setImageLink(element.select("a > img").attr("src").toString());
				model.setPrice(element.select("div.listing-detail > div.listing-price").text());
				String producLink = element.select("div.listing-detail > div.listing-title > a").attr("href").toString();
				model.setProductLink(producLink);
				document = getDocument(producLink);
				String desc = document.select("#description-text").first().text();
				desc = desc.length() > 1000 ? desc.substring(1, 1000) : desc;
				model.setDescription(desc);
				listData.add(model);
				console(model.toString());
				count++;
				if (count > 100)
					break;

			}

		}
		console(new Gson().toJson(listData));

		
		
		
	}

	@Override
	public void exportData() {
		// TODO Auto-generated method stub

	}

}
