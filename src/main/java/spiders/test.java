/**
 * 
 */
package spiders;

import java.io.File;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.Platform;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.phantomjs.PhantomJSDriverService;
import org.openqa.selenium.remote.BrowserType;
import org.openqa.selenium.remote.DesiredCapabilities;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import core.selenium.factory.WebDriverController;
import core.selenium.factory.WebDriverFactory;
import model.B01Model;

/**
 * @author thi
 *
 */
public class test {
	public void doSomething() {
		DesiredCapabilities capabilities = new DesiredCapabilities();
		capabilities.setCapability("phantomjs.binary.path", "C:/phantomjs-1.9.8-windows/phantomjs.exe");
		capabilities.setBrowserName(BrowserType.PHANTOMJS);
		capabilities.setPlatform(Platform.ANY);

		WebDriver driver = WebDriverFactory.getDriver(capabilities);
		driver.get("http://markets.ft.com/research/Markets/Commodities");
		// new WebDriverWait(driver,
		// 3).until(ExpectedConditions.visibilityOfElementLocated(By.c("sortableTable")));
		WebElement tab3 = driver.findElement(By.cssSelector("li[w_action=Commodities_Tab_ArgicultureAndLumber]"));
		System.out.println(tab3.getAttribute("w_action"));
		tab3.click();
		try {
			Thread.sleep(2000);
		} catch (InterruptedException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		WebElement element = driver.findElement(By.className("sortableTable"));

		System.out.println(element.findElement(By.cssSelector("tbody>tr>td.firstCol > div > a")).getAttribute("href"));

		// driver.close();
	}

	public void getEbay(String value) {
		WebDriver driver = null;
		try {
			DesiredCapabilities capabilities = new DesiredCapabilities();
			/*
			 * capabilities.setCapability(
			 * PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
			 * utils.FileUtils.getJarDir() + File.separator + "phantomjs");
			 */
			/*
			 * capabilities.setCapability(
			 * PhantomJSDriverService.PHANTOMJS_EXECUTABLE_PATH_PROPERTY,
			 * "C:/phantomjs.exe");
			 */

			/*
			 * System.setProperty("webdriver.ie.driver",
			 * "C:/IEDriverServer.exe");
			 */
			System.setProperty("webdriver.chrome.driver", "C:/chromedriver.exe");
			capabilities.setJavascriptEnabled(true);
			capabilities.setCapability("takesScreenshot", false);
			capabilities.setBrowserName(BrowserType.CHROME);
			capabilities.setPlatform(Platform.ANY);
			capabilities.setCapability(PhantomJSDriverService.PHANTOMJS_CLI_ARGS,
					new String[] { "--web-security=false", "--ssl-protocol=any", "--ignore-ssl-errors=true",
							"--load-images=no", "--disk-cache=true", "--output-encoding=utf-8" });

			driver = WebDriverFactory.getDriver(capabilities);
			// driver =
			// WebDriverFactory.getDriver(DesiredCapabilities.firefox());

			driver.manage().timeouts().implicitlyWait(180, TimeUnit.SECONDS);

			driver.get("http://www.ebay.com/");
			driver.findElement(By.cssSelector("#gh-ac")).sendKeys(value);

			WebDriverController controller = new WebDriverController(driver);

			driver.findElement(By.cssSelector("#gh-btn")).click();

			 driver.manage().timeouts().implicitlyWait(30,TimeUnit.SECONDS);
			 WebElement listViewInner = driver.findElement(By.cssSelector("#ListViewInner"));
			 List<WebElement> listItem =  driver.findElements(By.className("li"));
			 List<B01Model> listModel = new ArrayList<B01Model>();
			 int count = 0;
			 for (WebElement webElement : listItem) {
				if (count>10) break;
				B01Model model = new B01Model();
				model.setTitle(webElement.findElement(By.cssSelector("h3")).getText());
				model.setImageLink(webElement.findElement(By.cssSelector("div > div > a > img")).getAttribute("src"));
				model.setDescription("");
				model.setPrice(webElement.findElement(By.cssSelector("ul.lvprices.left.space-zero > li.lvprice.prc > span")).getText());
				String productLink = webElement.findElement(By.cssSelector("h3 > a")).getAttribute("href");
				model.setProductLink(productLink);
				System.out.println(model.toString());
				count++;
			}

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			driver.close();

		}

	}

	
	public void getEbay100(String key) {
		
	}
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		test tester = new test();
		tester.getEbay("moroccan");
	}

}
